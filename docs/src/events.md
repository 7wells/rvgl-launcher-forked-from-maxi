# Joining Events

<!-- toc -->

[![Events tab](screens/events.th.png)](screens/events.png)

## Event List

The `Events` tab lists current and upcoming online events. Events are sourced
from Re-Volt I/O and other third-party repositories. You can see the list of
available repositories and add more from the [Repositories tab](./repos.md).

> The date and time of each event is displayed in UTC. Use
[timeanddate.com](https://www.timeanddate.com) if you want to convert it to
your local time zone. The launcher also displays a status message in human
readable format based on your local time zone.

> Double click an event to open the event's page in your default browser.

## Event States

Events can be in one of these states:

- **Upcoming**: The event hasn't started yet.
- **In Progress**: The event is currently being played.
- **Ended**: The event has ended.

> The launcher has no way as of yet to query the actual state of an event.
Event states are guessed based on the listed date and time. An event is counted
as `In Progress` up to 2 hours after the listed time.

> Use `Update Event Info` to refresh the event information. It's automatically
refreshed once every 15 minutes.

## Package Selection

Each event has its own preset based on packages required by the event. You can
customize this preset further and enable any optional packs, such as music or
skins. Select an event and use the `Select Packs` button to edit the event's
preset.

## Event Lobbies

An event has one or more lobbies. Select an event and then join one of the
available lobbies by clicking the `Join` button. This will launch RVGL with
the appropriate preset and connect directly to the event's lobby.

> You can attempt to join an event at any time. If the event is not active at
the time of joining, the connection will time out.

