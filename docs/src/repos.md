# Repositories

<!-- toc -->

[![Repositories tab](screens/repos.th.png)](screens/repos.png)

## Adding Repositories

Repositories are a collection of packages and events provided by a source, typically
a web server. The default repository provides base game data, RVGL packages and
I/O content. The launcher downloads an additional set of trusted repositories
from [GitLab](https://gitlab.com/re-volt/rvio/repos). These are listed in the
`Repositories` tab.

Repositories are created by package maintainers and event hosters. As a user, you
can add a repository by pasting the complete URL of the repository into the provided
textbox and clicking `Add Repository`.

A newly added repository is initially disabled. Tick the checkboxes to the left
of each repository to enable them. Double a repository to open the url in your
browser.

> Enabled repositories are not fetched immediately. Use `Update Repository Info`
to refresh repository data. It's automatically refreshed once every 15 minutes.

## Repository File

> This section is meant for package maintainers and event hosters who want to
create their own repositories.

Each repository is a JSON file with the name, version, packages and events data.
Upload the JSON file somewhere for other people to access. You must send them the
URL (like `https://hajduc.com/downloads/repo.json`). Players can then add this
repository to their launcher. Below is a sample repository file:

```json
{
    "name": "hajduc's Repo",
    "version": "21.0115",
    "packages": {
        "no-repo-trackpack": {
            "description": "Tracks, where your goal is to not fall.",
            "version": "20.0408",
            "checksum": "5842a8398e3e55aa520631825e207b984f6204fe60240a866eb650437f4fadfe",
            "url": "https://hajduc.com/downloads/no_repo_tracks_200408.zip",
            "file": "no_repo_tracks_200408.zip"
        },
        "twc-week1": {
            "description": "Toyeca Winter Cup - Week 1",
            "version": "21.0107",
            "checksum": "aa27b6f2cfdd671680daacb47f19e72aa0e891a392cadf5cfb0d30fc5ba107d6",
            "url": "https://hajduc.com/downloads/2021twc_week1.zip",
            "file": "2021twc_week1.zip"
        },
        "twc-week2": {
            "description": "Toyeca Winter Cup - Week 2",
            "version": "21.0113",
            "checksum": "37d7990ffffc46dd61455e23010923ae737656949f44925e33e4509007043b63",
            "url": "https://hajduc.com/downloads/2021twc_week2.zip",
            "file": "2021twc_week2.zip"
        }
    },
    "events": {
        "event-01": {
            "title": "Fullspeed!",
            "date": "07-02-2021 15:00",
            "url": "https://re-volt.io/events/event-01",
            "hosts": {
                "Max": "192.168.178.1"
            },
            "category": "casual",
            "mode": "race",
            "class": "superpro"
        },
        "event-02": {
            "title": "Pure event",
            "date": "07-02-2021 18:00",
            "url": "https://re-volt.io/events/event-02",
            "hosts": {
                "Max": "192.168.178.20",
                "Wusel": "192.168.178.2"
            },
            "category": "casual",
            "mode": "race",
            "class": "advanced"
        }
    }
}
```

A repository can contain a `packages` section, an `events` section, or both.

### Packages Section

`url` must link directly to the package file. For example, Dropbox links must
include `?dl=1`. Google Drive links may not be supported. Packages can be provided
in `zip`, `7z` or `rar` formats.

`file` is the package file name. If it's not provided, the package name is used
as the file name, and file type (extension) is deduced from the URL. For example,
if the package name is `twc-week1` and URL is `https://hajduc.com/downloads/2021twc_week1.zip`,
the downloaded package is saved as `twc-week1.zip`. The `file` key is only needed
if the URL doesn't contain the file name.

`checksum` is optional, but it is highly recommended. It must be a SHA-256 hash
of the package file. If provided, it's used to verify integrity of the downloaded
package.

Repository providers must take responsibility for:

- The availability of packages.
- File size (avoid BMP files; compress them to optimal PNGs or JPGs).
- Cleanliness of the package (only include game files; no unwanted files or malware).
- Make sure that **all** included files and folders have **lowercase names**.

### Events Section

`date` must be provided in UTC and in exactly the same format shown in the example,
i.e., `DD-MM-YYYY HH:MM`. Hours are in 24-hour format, i.e., `19:00` for 7 PM.
Use [timeanddate.com](https://www.timeanddate.com) to convert your time to UTC.

`hosts` contains the list of people hosting the event and the IP address
for joining their game.

`category` can be any of these:
- `casual`
- `competitive`
- `special`
- `tourney`
- `unofficial`
- `other`

`mode` can be any of these:
- `race`
- `battle-tag`
- `last-man-standing`
- `volt-ball`
- `other`

`class` can be any of these:
- `rookie`
- `amateur`
- `advanced`
- `semi-pro`
- `pro`
- `superpro`
- `other`
