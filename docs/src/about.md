# About

RVGL Launcher is work-in-progress and actively developed by the RVGL team and
members of Re-Volt I/O community. The project is open-source and licensed under
GPLv3.

[Visit the GitLab Project Page](https://gitlab.com/re-volt/rvgl-launcher).

Developers: *Marv* and *Huki*.  
Contributors: *vonunwerth*.  
Bolt Icon by *URV*.

