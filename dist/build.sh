#!/bin/bash

for i in "$@"; do
  case $i in
    --linux) linux=linux;;
    --win32) win32=win32; gcc=i686-w64-mingw32-gcc;;
    --win64) win32=win64; gcc=x86_64-w64-mingw32-gcc;;
    --build) build=true;;
    --clean) clean=true;;
    --helper) helper=true;;
    --release) release=true;;
  esac
done

if [ "$helper" = true ]; then
  if [ "$win32" ]; then
    ${gcc} -o ${win32}/helper.exe helper.cpp -lole32 -luuid -s
  fi
  exit
fi

if [ "$clean" = true ]; then
  rm -f *.zip *.gz *.txt *.json
  rm -rf win32/build
  rm -rf win64/build
  rm -f win32/rvgl_launcher.exe
  rm -f win64/rvgl_launcher.exe
  rm -f win32/helper.exe
  rm -f win64/helper.exe
  exit
fi

if [ "$build" = true ]; then
  python=python
  if [ "$PYTHONPATH" ]; then
    python="${PYTHONPATH}/python"
  fi
  python version.py # Always use native python
  if [ "$win32" ]; then
    "${python}.exe" -m PyInstaller rvgl_launcher.spec --workpath ${win32}/build --distpath ${win32}
  fi
  exit
fi

if [ "$win32" ]; then
  rm -f rvgl_launcher_${win32}.zip
  (cd ${win32} && zip -r ../rvgl_launcher_${win32}.zip * -x "build*" "*.spec")
  (cd .. && zip -r dist/rvgl_launcher_${win32}.zip icons repos)
fi

if [ "$linux" ]; then
  rm -f rvgl_launcher_${linux}.zip
  files="icons repos rv_launcher appdirs.py rvgl_launcher.py"
  (cd .. && zip -r dist/rvgl_launcher_${linux}.zip ${files} -x "*__pycache__*")
fi

if [ "$release" = true ]; then
  python release.py # Always use native python
fi
