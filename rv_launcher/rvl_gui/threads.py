import wx
import threading

from rv_launcher import common
from rv_launcher.helpers import *
from rv_launcher.logging import *
from rv_launcher.network import *
from rv_launcher.updates import install_update
from rv_launcher.recipes import Recipe


wxEVT_UPDATE_PACK_INFO = wx.NewEventType()
wxEVT_UPDATE_EVENT_INFO = wx.NewEventType()
wxEVT_LAUNCH_GAME = wx.NewEventType()
wxEVT_DOWNLOAD_PROGRESS = wx.NewEventType()
wxEVT_DOWNLOAD_GAME = wx.NewEventType()
wxEVT_UPDATE_GAME = wx.NewEventType()
wxEVT_CLEANUP_GAME = wx.NewEventType()
wxEVT_SHOW_MESSAGE = wx.NewEventType()
wxEVT_INSTALL_LOCAL = wx.NewEventType()
wxEVT_UPDATE_APP = wx.NewEventType()
wxEVT_COLLECT_TASKS = wx.NewEventType()
wxEVT_IPC_ARGS = wx.NewEventType()

EVT_UPDATE_PACK_INFO = wx.PyEventBinder(wxEVT_UPDATE_PACK_INFO, 1)
EVT_UPDATE_EVENT_INFO = wx.PyEventBinder(wxEVT_UPDATE_EVENT_INFO, 1)
EVT_LAUNCH_GAME = wx.PyEventBinder(wxEVT_LAUNCH_GAME, 1)
EVT_DOWNLOAD_PROGRESS = wx.PyEventBinder(wxEVT_DOWNLOAD_PROGRESS, 1)
EVT_DOWNLOAD_GAME = wx.PyEventBinder(wxEVT_DOWNLOAD_GAME, 1)
EVT_UPDATE_GAME = wx.PyEventBinder(wxEVT_UPDATE_GAME, 1)
EVT_CLEANUP_GAME = wx.PyEventBinder(wxEVT_CLEANUP_GAME, 1)
EVT_SHOW_MESSAGE = wx.PyEventBinder(wxEVT_SHOW_MESSAGE, 1)
EVT_INSTALL_LOCAL = wx.PyEventBinder(wxEVT_INSTALL_LOCAL, 1)
EVT_UPDATE_APP = wx.PyEventBinder(wxEVT_UPDATE_APP, 1)
EVT_COLLECT_TASKS = wx.PyEventBinder(wxEVT_COLLECT_TASKS, 1)
EVT_IPC_ARGS = wx.PyEventBinder(wxEVT_IPC_ARGS, 1)


class ThreadEvent(wx.PyCommandEvent):
    def __init__(self, eventType, value=None):
        wx.PyCommandEvent.__init__(self, eventType, -1)
        self.value = value


class UpdatePackInfoThread(threading.Thread):
    def __init__(self, parent):
        threading.Thread.__init__(self, daemon=True)
        self.parent = parent

    def safe_run(self):
        common.repo.update_package_info()

    def run(self):
        try:
            self.safe_run()
        except Exception as e:
            print_trace()

        event = ThreadEvent(wxEVT_UPDATE_PACK_INFO)
        wx.PostEvent(self.parent, event)


class UpdateEventInfoThread(threading.Thread):
    def __init__(self, parent):
        threading.Thread.__init__(self, daemon=True)
        self.parent = parent

    def safe_run(self):
        common.event_catalog.download_events()

    def run(self):
        try:
            self.safe_run()
        except Exception as e:
            print_trace()

        event = ThreadEvent(wxEVT_UPDATE_EVENT_INFO)
        wx.PostEvent(self.parent, event)


class LaunchGameThread(threading.Thread):
    def __init__(self, parent, recipe, args=""):
        threading.Thread.__init__(self, daemon=True)
        self.parent = parent
        self.recipe = recipe
        self.args = args

    def safe_run(self):
        self.recipe.cook()
        launch_game(self.recipe.slug, self.args)

    def run(self):
        try:
            self.safe_run()
        except Exception as e:
            print_trace()

        event = ThreadEvent(wxEVT_LAUNCH_GAME)
        wx.PostEvent(self.parent, event)


class DownloadGameThread(threading.Thread):
    def __init__(self, parent, recipe, shortcut=False):
        threading.Thread.__init__(self)
        self.parent = parent
        self.recipe = recipe
        self.shortcut = shortcut

    def safe_run(self):
        if self.shortcut:
            create_shortcut()
            register_uri()

        packages = self.recipe.packages.copy()
        common.recipes["default"] = Recipe(packages, "Default", local=True,
            writable=self.recipe.writable, root=self.recipe.root)
        common.recipes["default"].save()

        for progress in common.recipes["default"].prepare():
            event = ThreadEvent(wxEVT_DOWNLOAD_PROGRESS, progress)
            wx.PostEvent(self.parent, event)

        common.recipes["default"].cook()

        CONFIG["installed"] = True

    def run(self):
        try:
            self.safe_run()
        except Exception as e:
            print_trace()

        event = ThreadEvent(wxEVT_DOWNLOAD_GAME)
        wx.PostEvent(self.parent, event)


class UpdateGameThread(threading.Thread):
    def __init__(self, parent, clean=False, uninstall=False, recipes=[]):
        threading.Thread.__init__(self)
        self.parent = parent
        self.clean = clean
        self.uninstall = uninstall
        self.recipes = recipes

    def safe_run(self):
        if self.clean or self.uninstall:
            for recipe in self.recipes:
                recipe.clean()

        if self.uninstall:
            CONFIG["installed"] = False
            return

        for recipe in self.recipes:
            for progress in recipe.prepare():
                event = ThreadEvent(wxEVT_DOWNLOAD_PROGRESS, progress)
                wx.PostEvent(self.parent, event)

            recipe.cook()

    def run(self):
        try:
            self.safe_run()
        except Exception as e:
            print_trace()

        event = ThreadEvent(wxEVT_UPDATE_GAME)
        wx.PostEvent(self.parent, event)


class CleanupGameThread(threading.Thread):
    def __init__(self, parent, packs=False, dloads=False, logs=False):
        threading.Thread.__init__(self)
        self.parent = parent
        self.packs = packs
        self.dloads = dloads
        self.logs = logs

    def safe_run(self):
        if self.packs:
            self.parent.clean_packs()

        if self.dloads:
            clean_folder(os.path.join(get_data_dir(), "downloads"))
            clean_folder(os.path.join(get_data_dir(), "update"))

        if self.logs:
            content = common.log_file.load()
            clean_folder(os.path.join(CONFIG_DIR, "logs"))
            common.log_file.save(content)

    def run(self):
        try:
            self.safe_run()
        except Exception as e:
            print_trace()

        event = ThreadEvent(wxEVT_CLEANUP_GAME)
        wx.PostEvent(self.parent, event)


class InstallLocalThread(threading.Thread):
    def __init__(self, parent, files, download=False):
        threading.Thread.__init__(self)
        self.parent = parent
        self.files = files
        self.download = download

    def safe_run(self):
        for name, url in self.files.items():
            for progress, result in common.repo.install_local(name, url, self.download):
                event = ThreadEvent(wxEVT_DOWNLOAD_PROGRESS, progress)
                wx.PostEvent(self.parent, event)

                if isinstance(result, dict):
                    message_event.clear()
                    event = ThreadEvent(wxEVT_SHOW_MESSAGE, result)
                    wx.PostEvent(self.parent, event)
                    message_event.wait()

            event = ThreadEvent(wxEVT_INSTALL_LOCAL, result)
            wx.PostEvent(self.parent, event)

    def run(self):
        try:
            self.safe_run()
        except Exception as e:
            print_trace()

        event = ThreadEvent(wxEVT_INSTALL_LOCAL)
        wx.PostEvent(self.parent, event)


class UpdateAppThread(threading.Thread):
    def __init__(self, parent):
        threading.Thread.__init__(self)
        self.parent = parent

    def safe_run(self):
        for status in install_update():
            event = ThreadEvent(wxEVT_UPDATE_APP, status)
            wx.PostEvent(self.parent, event)

            if not status:
                # Wait for application to exit
                close_event.wait()

    def run(self):
        try:
            self.safe_run()
        except Exception as e:
            print_trace()

        event = ThreadEvent(wxEVT_UPDATE_APP, "")
        wx.PostEvent(self.parent, event)


class CollectTasksThread(threading.Thread):
    def __init__(self, parent):
        threading.Thread.__init__(self)
        self.parent = parent

    def safe_run(self):
        for worker in self.parent.get_all_workers():
            if worker and worker.is_alive():
                worker.join()

    def run(self):
        try:
            self.safe_run()
        except Exception as e:
            print_trace()

        event = ThreadEvent(wxEVT_COLLECT_TASKS)
        wx.PostEvent(self.parent, event)


class IpcListenerThread(threading.Thread):
    def __init__(self, name, frame):
        threading.Thread.__init__(self, daemon=True)
        self.name = name
        self.frame = frame

    def safe_run(self):
        ipc = IpcHelper(self.name)
        for msg in ipc.server():
            args = msg.get("args", [])
            event = ThreadEvent(wxEVT_IPC_ARGS, args)
            wx.PostEvent(self.frame, event)

    def run(self):
        try:
            self.safe_run()
        except Exception as e:
            print_trace()

