import wx
import os
import sys
import re
import webbrowser
from difflib import SequenceMatcher

from rv_launcher.rvl_gui.threads import *
from rv_launcher.rvl_gui.widgets import *
from rv_launcher.common import *
from rv_launcher.helpers import *
from rv_launcher.startup import *
from rv_launcher.logging import *
from rv_launcher.backup import *


class GameTab(ScrolledTabPage):
    def __init__(self, parent):
        TabPage.__init__(self, parent)
        self.init_ui()

    def init_ui(self):
        self.box = wx.BoxSizer(wx.VERTICAL)

        # Install pane
        self.init_install_ui()

        # Launch pane
        self.init_launch_ui()

        self.box.Add(self.box_install, 1, wx.EXPAND)
        self.box.Add(self.box_launch, 1, wx.EXPAND)

        # Events
        self.status_text = "Installing game..."
        self.Bind(EVT_DOWNLOAD_GAME, self.on_download_and_install_done)

        # Show
        self.set_install_type(CONFIG["install-type"])
        self.set_install_preset(CONFIG["install-recipe"])
        self.set_preset(CONFIG["default-recipe"])

        if game_installed():
            self.show_launch()
        else:
            self.show_install()

        self.SetSizer(self.box)

    def init_install_ui(self):
        self.box_install = wx.BoxSizer(wx.VERTICAL)

        self.install_types = {
            "Standard": "Let the launcher manage your game data.",
            "Custom": "Pick a different install location or point to an existing install."
        }

        install_types = list(self.install_types)
        text = wx.StaticText(self, -1, "The game doesn't seem to be installed. Select your install type:")
        self.box_install.Add(text, 0, wx.ALIGN_CENTER | wx.ALL, 15)

        self.rbox_type = wx.RadioBox(self, label='Install Type', choices=install_types,
            majorDimension=2, style=wx.RA_SPECIFY_COLS)
        self.rbox_type.Bind(wx.EVT_RADIOBOX, self.on_install_type_selection)
        self.box_install.Add(self.rbox_type, 0, wx.ALIGN_CENTER | wx.ALL, 15)

        selection = install_types[self.rbox_type.GetSelection()]
        self.desc_type = wx.StaticText(self, label=self.install_types[selection])
        self.box_install.Add(self.desc_type, 0, wx.ALIGN_CENTER | wx.ALL, 15)

        self.box_install_location = wx.BoxSizer(wx.HORIZONTAL)
        self.ctrl_path = wx.TextCtrl(self, style=wx.TE_READONLY)
        self.ctrl_path.SetMinSize(wx.Size(480, 30))
        self.box_install_location.Add(self.ctrl_path, 0, wx.ALL | wx.EXPAND, 5)

        self.button_browse = wx.Button(self, -1, "Browse...")
        self.button_browse.Bind(wx.EVT_BUTTON, self.on_install_path_selection)
        self.box_install_location.Add(self.button_browse, 0, wx.ALL | wx.EXPAND, 5)

        self.box_install.Add(self.box_install_location, 0, wx.ALIGN_CENTER | wx.ALL, 15)
        self.box_install.AddStretchSpacer()

        self.text_install = wx.StaticText(self, -1, "Select your preferred preset:")
        self.box_install.Add(self.text_install, 0, wx.ALIGN_CENTER | wx.ALL, 15)

        self.box_install_preset = wx.BoxSizer(wx.VERTICAL)
        self.install_recipes = [s for s, r in common.recipes.items() if r.install]
        choices = [common.recipes[r].title for r in self.install_recipes]
        self.rbox_preset = wx.RadioBox(self, label='Game Preset', choices=choices,
            majorDimension=2, style=wx.RA_SPECIFY_COLS)
        self.rbox_preset.Bind(wx.EVT_RADIOBOX, self.on_install_preset_selection)
        self.box_install_preset.Add(self.rbox_preset, 0, wx.ALIGN_CENTER | wx.ALL, 15)

        recipe = self.install_recipes[self.rbox_preset.GetSelection()]
        self.desc_preset = wx.StaticText(self, label=common.recipes[recipe].description)
        self.box_install_preset.Add(self.desc_preset, 0, wx.ALIGN_CENTER | wx.ALL, 15)

        self.box_install.Add(self.box_install_preset, 0, wx.EXPAND, 0)
        self.box_install.AddStretchSpacer()

        self.box_install_footer = wx.BoxSizer(wx.HORIZONTAL)
        self.chbox_shortcut = wx.CheckBox(self, label=self.get_shortcut_label())
        self.chbox_shortcut.SetValue(True)
        self.box_install_footer.Add(self.chbox_shortcut, 1, wx.ALIGN_CENTER | wx.ALL, 0)

        self.button_install = wx.Button(self, -1, self.get_install_label())
        self.button_install.SetMinSize(wx.Size(160, 30))
        self.button_install.Bind(wx.EVT_BUTTON, self.on_download_and_install)
        self.box_install_footer.Add(self.button_install, 1, wx.ALIGN_CENTER | wx.ALL, 0)
        self.box_install_footer.AddStretchSpacer()

        self.box_install.Add(self.box_install_footer, 0, wx.EXPAND | wx.ALL, 15)

    def init_launch_ui(self):
        self.box_launch = wx.BoxSizer(wx.VERTICAL)
        self.box_header = wx.GridSizer(rows=1, cols=2, vgap=30, hgap=30)

        self.box_packs = wx.BoxSizer(wx.VERTICAL)
        self.box_preset = wx.BoxSizer(wx.HORIZONTAL)
        text = wx.StaticText(self, -1, "Select Preset:")
        self.box_preset.Add(text, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)

        self.recipes = [s for s, r in common.recipes.items() if r.local]
        self.choice_preset = wx.Choice(self, -1, choices=self.recipes)
        self.choice_preset.Bind(wx.EVT_CHOICE, self.on_preset_selection)
        self.box_preset.Add(self.choice_preset, 1, wx.EXPAND | wx.ALL, 5)
        self.box_packs.Add(self.box_preset, 0, wx.EXPAND, 0)

        self.pack_list = wx.ListCtrl(self, -1, style=wx.LC_LIST)
        self.pack_list.SetMinSize(wx.Size(300, 50))
        self.pack_list.SetSize(wx.Size(300, 50))
        self.box_packs.Add(self.pack_list, 1, wx.EXPAND | wx.ALL, 5)

        self.box_info = wx.StaticBoxSizer(wx.VERTICAL, self, "Information")
        self.box_info.SetMinSize(wx.Size(300, 50))
        static_box = self.box_info.GetStaticBox()

        self.box_packs_message = wx.BoxSizer(wx.HORIZONTAL)
        self.packs_message = wx.StaticText(static_box, -1, "All packages are up to date.")
        self.packs_button = wx.Button(static_box, -1, "View")
        self.packs_button.Bind(wx.EVT_BUTTON, self.on_view_packs)
        self.box_packs_message.Add(self.packs_message, 1, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)
        self.box_packs_message.Add(self.packs_button, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)
        self.box_info.Add(self.box_packs_message, 1, wx.EXPAND | wx.ALL, 0)

        self.box_events_message = wx.BoxSizer(wx.HORIZONTAL)
        self.events_message = wx.StaticText(static_box, -1, "There are no upcoming events.")
        self.events_button = wx.Button(static_box, -1, "View")
        self.events_button.Bind(wx.EVT_BUTTON, self.on_view_events)
        self.box_events_message.Add(self.events_message, 1, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)
        self.box_events_message.Add(self.events_button, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)
        self.box_info.Add(self.box_events_message, 1, wx.EXPAND | wx.ALL, 0)

        self.box_rvgl_message = wx.BoxSizer(wx.HORIZONTAL)
        self.rvgl_message = wx.StaticText(static_box, -1, "RVGL is up to date.")
        self.rvgl_button = wx.Button(static_box, -1, "Update")
        self.rvgl_button.Bind(wx.EVT_BUTTON, self.on_update_rvgl)
        self.rvgl_button.Disable()
        self.box_rvgl_message.Add(self.rvgl_message, 1, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)
        self.box_rvgl_message.Add(self.rvgl_button, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)
        self.box_info.Add(self.box_rvgl_message, 1, wx.EXPAND | wx.ALL, 0)

        self.box_header.Add(self.box_packs, 1, wx.EXPAND, 0)
        self.box_header.Add(self.box_info, 0, wx.ALIGN_RIGHT | wx.ALL, 5)
        self.box_launch.Add(self.box_header, 0, wx.EXPAND | wx.ALL, 10)
        self.box_launch.AddStretchSpacer()

        self.box_footer = wx.BoxSizer(wx.VERTICAL)
        self.box_params = wx.BoxSizer(wx.HORIZONTAL)
        text = wx.StaticText(self, label="Launch Parameters")
        self.box_params.Add(text, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)

        self.ctrl_params = wx.TextCtrl(self)
        self.ctrl_params.Bind(wx.EVT_TEXT, self.on_set_params)
        self.ctrl_params.ChangeValue(CONFIG["launch-params"])
        self.box_params.Add(self.ctrl_params, 1, wx.ALL | wx.EXPAND, 5)
        self.box_footer.Add(self.box_params, 0, wx.EXPAND, 0)

        self.button_launch = wx.Button(self, -1, "Launch RVGL")
        self.button_launch.SetMinSize(wx.Size(160, 35))
        self.button_launch.Bind(wx.EVT_BUTTON, self.on_launch_game)
        self.box_footer.Add(self.button_launch, 0, wx.ALL | wx.EXPAND, 5)
        self.box_launch.Add(self.box_footer, 0, wx.ALL | wx.EXPAND, 10)

    """ Hides installer elements and shows the game elements """
    def show_launch(self):
        self.box.Hide(self.box_install)
        self.box.Show(self.box_launch)
        self.Layout()

    """ Hides game elements and shows the installer elements """
    def show_install(self):
        self.box.Hide(self.box_launch)
        self.box.Show(self.box_install)
        self.refresh_install()
        self.Layout()

    """ Returns labels for installer elements """
    def get_shortcut_label(self):
        if sys.platform == "win32":
            return "Desktop Shortcut"
        else:
            return "App Menu Shortcut"

    def get_install_label(self):
        recipe = self.install_recipes[self.rbox_preset.GetSelection()]
        if recipe == "custom":
            return "Select Packs"
        else:
            return "Download and Install"

    """ Updates text in the information pane """
    def set_packs_message(self, text):
        self.packs_message.SetLabel(text)
        self.Layout()

    def set_events_message(self, text):
        self.events_message.SetLabel(text)
        self.Layout()

    def set_rvgl_message(self, text):
        self.rvgl_message.SetLabel(text)
        self.Layout()

    """ Functions for managing installation """
    def set_install_type(self, install):
        install_types = [i.lower() for i in self.install_types]
        if install in install_types:
            self.rbox_type.SetSelection(install_types.index(install))
            self.on_install_type_selection(None)

    def set_install_path(self):
        self.ctrl_path.ChangeValue(get_data_dir())
        enable = CONFIG["install-type"] != "standard"
        self.ctrl_path.Enable(enable)
        self.button_browse.Enable(enable)

    def refresh_install(self):
        if find_game(get_data_dir()):
            self.box_install.Hide(self.box_install_preset)
            self.text_install.SetLabel("RVGL is already installed in this location.")
            self.set_install_preset("custom")
        else:
            self.box_install.Show(self.box_install_preset)
            self.text_install.SetLabel("Select your preferred preset:")
            self.set_install_preset("original")

        self.Layout()

    def prepare_install(self):
        data_dir = get_data_dir()
        if not os.path.isdir(data_dir):
            message = "Installation directory does not exist!"
            self.frame.show_message("Error", message)
            return False

        prepare_folders()

        if CONFIG["install-type"] == "standard":
            return True

        CONFIG["launch-dir"] = common.repo.get_package_dir(f"rvgl_{PLATFORM}")
        CONFIG["save-dir"] = os.path.join(data_dir, "save")

        if find_game(data_dir):
            CONFIG["launch-dir"] = data_dir
        if find_save(data_dir):
            CONFIG["save-dir"] = data_dir

        return True

    """ Functions for managing presets """
    def set_install_preset(self, recipe):
        if recipe in self.install_recipes:
            self.rbox_preset.SetSelection(self.install_recipes.index(recipe))
            self.on_install_preset_selection(None)

    def set_preset(self, recipe):
        recipe = ("default", recipe)[recipe in self.recipes]
        if recipe in self.recipes:
            self.choice_preset.SetSelection(self.recipes.index(recipe))
            self.on_preset_selection(None)

    def refresh_presets(self):
        self.recipes = [s for s, r in common.recipes.items() if r.local]
        self.choice_preset.Clear()
        self.choice_preset.Append(self.recipes)
        self.set_preset(CONFIG["default-recipe"])

    """ Event handlers """
    def on_view_packs(self, e):
        index = self.parent.FindPage(self.frame.packs_tab)
        self.parent.SetSelection(index)

    def on_view_events(self, e):
        index = self.parent.FindPage(self.frame.events_tab)
        self.parent.SetSelection(index)

    def on_update_rvgl(self, e):
        recipe = common.recipes["update"]
        self.frame.packs_tab.on_update_game(e, recipes=[recipe])

    def on_set_params(self, e):
        args = self.ctrl_params.GetValue()
        CONFIG["launch-params"] = args

    def on_launch_game(self, e):
        recipe = CONFIG["default-recipe"]
        self.frame.launch_game(common.recipes[recipe])

    def on_install_path_selection(self, e):
        message = "Choose game directory"
        with wx.DirDialog(self, message, CONFIG["data-dir"], style=wx.DD_DEFAULT_STYLE) as dialog:
            if dialog.ShowModal() == wx.ID_OK:
                CONFIG["data-dir"] = dialog.GetPath()
                self.on_install_type_selection(e)

    def on_install_type_selection(self, e):
        install_types = list(self.install_types)
        selection = install_types[self.rbox_type.GetSelection()]
        self.desc_type.SetLabelText(self.install_types[selection])
        CONFIG["install-type"] = selection.lower()
        self.set_install_path()

        if e:
            self.refresh_install()
            self.frame.refresh_packs(local=True)

        self.Layout()

    def on_install_preset_selection(self, e):
        recipe = self.install_recipes[self.rbox_preset.GetSelection()]
        self.desc_preset.SetLabelText(common.recipes[recipe].description)
        self.button_install.SetLabel(self.get_install_label())
        CONFIG["install-recipe"] = recipe

        self.Layout()

    def on_preset_selection(self, e):
        recipe = self.recipes[self.choice_preset.GetSelection()]
        self.pack_list.DeleteAllItems()
        for pack in common.recipes[recipe].get_packages():
            self.pack_list.Append([pack])
        CONFIG["default-recipe"] = recipe

        self.Layout()

    def on_download_and_install(self, e):
        if self.frame.get_worker():
            print_log("Worker thread is currently busy!")
            return

        recipe = self.install_recipes[self.rbox_preset.GetSelection()]
        if e and recipe == "custom":
            index = self.parent.FindPage(self.frame.packs_tab)
            self.parent.SetSelection(index)
            return

        if not self.prepare_install():
            return

        recipe = common.recipes[recipe]
        shortcut = self.chbox_shortcut.GetValue()

        self.frame.disable_game()
        self.frame.disable_packs()

        print_log(self.status_text)
        self.frame.set_status(self.status_text)

        worker = DownloadGameThread(self, recipe, shortcut)
        self.frame.set_worker(worker)
        worker.start()

    def on_download_and_install_done(self, e):
        self.frame.clear_status(self.status_text)
        print_log(self.status_text, "Done!")

        self.frame.set_worker(None)

        self.show_launch()
        self.frame.packs_tab.show_controls()
        self.frame.events_tab.show_controls()

        self.frame.enable_game()
        self.frame.enable_packs()
        self.frame.packs_tab.refresh_packs()

        index = self.parent.FindPage(self)
        self.parent.SetSelection(index)


class EventContextMenu(wx.Menu):

    def __init__(self, parent, event_object):
        super(EventContextMenu, self).__init__()
        self.parent = parent
        mmi = wx.MenuItem(self, wx.NewId(), 'Generate Trainings Cup')
        self.Append(mmi)
        self.Bind(wx.EVT_MENU, lambda event: self.parent.generate_trainings_cup(event, event_object), mmi)


class EventsTab(TabPage):
    def __init__(self, parent):
        TabPage.__init__(self, parent)
        self.init_ui()

    def generate_trainings_cup(self, event, event_object):
        if len(TRACK_FOLDER_NAME_DICT.keys()) == 0:  # if there are any track entries
            print_log("Re-Volt needs to be installed first. If you alreday installed the game, check if the levels can be found.")
            return
        event_list = common.event_catalog.get_event_names()
        event = event_list[event_object]
        tracklist = common.event_catalog.events[event].get("tracklist", [])  # Get tracklist of the event
        if len(tracklist) > 0 and tracklist[0] != "TBA":  # if there is no tracklist, no cup will be generated
            cup = "Name\t\""
            cup_title = "".join(x for x in common.event_catalog.events[event].get("title", "Traingscup") if x not in "\/:*?<>|")  # Remove symbols which are not allowed in a file name
            cup_date = common.event_catalog.events[event].get("date", "").replace(":", "-")  # replace : to do not have it in the folder path
            cup += cup_title + " " + cup_date + "\"\n"  # append name
            difficulty = 4
            cup += "Difficulty\t" + str(difficulty) + "\n"  # append difficulty
            obtain = 0
            cup += "Obtain\t" + str(obtain) + "\n\n"  # append obtain
            num_cars = 16
            cup += "NumCars\t" + str(num_cars) + "\n"  # append number of cars
            num_tries = 3
            cup += "NumTries\t" + str(num_tries) + "\n"  # Append number of tries

            event_class = common.event_catalog.events[event].get("class", "pro").lower()  # if class is not provided class will be set to pro
            rv_classes = ("rookie", "amateur", "advanced", "semi-pro", "pro", "super-pro")
            classes = [0] * 6
            if event_class in rv_classes:  # generate class list
                classes[rv_classes.index(event_class)] = num_cars - 1
            else:
                classes[4] = num_cars - 1
            cup += "Classes\t" + str(classes)[1:-1].replace(" ", "") + "\n"

            points = [0] * num_cars
            for i in range(num_cars - 1, 0, -1):
                points[num_cars - 1 - i] = i

            cup += "Points\t" + str(points)[1:-1].replace(" ", "") + "\n\n"  # append Points list

            cup_template = cup  # save current cup text to generate multiple cups for session with tracklists longer than 16

            tracklist_split = []  # split tracklist in chunks with the size of the maximum amount of tracks a cup can hold
            for i in range(0, int(len(tracklist) / CUP_MAX_TRACK_COUNT)):
                tracklist_split.append(tracklist[(i * CUP_MAX_TRACK_COUNT):((i + 1) * CUP_MAX_TRACK_COUNT)])  # append chunks taken from the tracklist with len of CUP_MAX_TRACK_COUNT
            tracklist_split.append(tracklist[len(tracklist_split) * CUP_MAX_TRACK_COUNT: len(tracklist)])  # append the lasting elements which are still not in the tracklist split

            chunk_count = ""
            for tracklist_chunk in tracklist_split:
                error_count = 0  # if more than two tracks are unknown, maybe a pack is missing, cup generation will stopped
                cup = cup_template
                track_count = 0
                for track_name in tracklist_chunk:  # append Track list
                    split_trackname = re.split('[-—]', track_name)  # split "Botanical Garden - 25 laps" in name and laps, also split if "dash" is used
                    try:
                        if len(split_trackname) > 1:  # people could perhaps forget to add a lap count, then set it to default 3
                            laps = split_trackname[-1]  # select last chunk - if there is a track like MK64 - Wario Stadium - 3 laps the lap count will be selected
                            laps = [int(s) for s in laps.split() if s.isdigit()][0]
                        else:
                            print_log("Could not parse the lap count for " + track_name + " Set to 3.")
                            laps = "3"
                    except:
                        laps = "3"
                        print_log("Could not parse the lap count for " + track_name + " Set to 3.")

                    track_name = "".join(split_trackname[:-1])
                    track_name = track_name.replace(" ", "")  # remove unnecessary spaces
                    reverse = "(R)" in track_name  # check if there is a reverse tag in the track name
                    track_name = track_name.replace("(R)", "")
                    mirrored = "(M)" in track_name  # check if there is a mirrored tag in the track name
                    track_name = track_name.replace("(M)", "")
                    best_match = 0
                    best_track_name = ""

                    folder_name_track_dict = {value: key for key in TRACK_FOLDER_NAME_DICT for value in TRACK_FOLDER_NAME_DICT[key]}  # invert dict search by the names
                    for track in TRACK_FOLDER_NAME_DICT.values():  # check which track is meant by for example "PetoVolt"
                        similarity = SequenceMatcher(None, track_name, track[0]).ratio()  # compare given string with all entries in the dict and find a "matching" one
                        if similarity > best_match:  # find best matching track
                            best_match = similarity
                            best_track_name = track[0]
                    best_track_folder_name = "nhood1"
                    if best_match < 0.45:  # threshold...
                        print_log("Tracks seems to be unknown: " + track_name + " (Similarity of " + str(round(best_match,
                                                                                                               2)) + " to " + best_track_name + ") - Will be replaced with Toys in the Hood 1")
                        best_track_name = "ToysintheHood1"  # default track, if track is not found. Will skip little errors in the tracklist
                        error_count += 1
                    try:
                        best_track_folder_name = folder_name_track_dict[best_track_name]
                    except KeyError:
                        pass # nhood1 as stays default
                    if error_count >= 3:  # if more than 3 tracks are not found, perhaps a pack is missing
                        print_log("Too much tracks could not be found. Cup generation aborted. Check if you own all necessary tracks and the tracklist entries are correct.")
                        return
                    print(folder_name_track_dict)
                    cup += "STAGE " + str(track_count) + "\t" + best_track_folder_name + " " + str(laps) + " " + str(mirrored) + " " + str(reverse) + "\n"
                    track_count += 1

                cup_file_path = os.path.join(DATA_DIR, "packs", "local", "cups", (cup_date + " " + cup_title + " " + str(chunk_count) + ".txt")).replace(" ",
                                                                                                                                                         "_")  # generate file path

                if not os.path.exists(os.path.dirname(cup_file_path)): # create cup folder if its not existing
                    try:
                        os.makedirs(os.path.dirname(cup_file_path))
                    except OSError as exc:  # Guard against race condition
                        print_log("Can not write to packs/local/cups. Check if folder can be written.")
                        return
                with open(cup_file_path, "w") as cup_file:
                    cup_file.write(cup)
                if chunk_count == "":
                    chunk_count = 2  # give the second chunk an appendix _2
                else:
                    chunk_count += 1
                print_log("Trainings Cup generated: " + cup_file.name)
                print_log(cup)
        else:
            print_log("No Tracklist provided. Can't generate a trainings cup for " + common.event_catalog.events[event].get("title", "the Session"))
            return

    def on_right_down(self, e):
        x, y = e.GetPoint()  # get current mouse coordinates
        self.PopupMenu(EventContextMenu(self, e.GetIndex()), (x + 5, y + 35))

    def init_ui(self):
        self.box = wx.BoxSizer(wx.VERTICAL)

        # Event list
        widths = [180, 180, 180, 220]
        self.event_list = ListCtrl(self, widths, -1, style=wx.LC_REPORT | wx.LC_SINGLE_SEL)
        self.event_list.InsertColumn(0, "Title", width=widths[0])
        self.event_list.InsertColumn(1, "Date", width=widths[1])
        self.event_list.InsertColumn(2, "Hosts", width=widths[2])
        self.event_list.InsertColumn(3, "Status", width=widths[3])

        self.event_list.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.on_double_click_event)
        self.event_list.Bind(wx.EVT_LIST_ITEM_SELECTED, self.on_select_event)
        self.event_list.Bind(wx.EVT_LIST_ITEM_DESELECTED, self.on_select_event)
        self.event_list.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.on_right_down)

        # Main toolbar
        self.box_toolbar = wx.BoxSizer(wx.HORIZONTAL)
        self.button_update_event_info = wx.Button(self, -1, "Update Event Info")
        self.button_update_event_info.Bind(wx.EVT_BUTTON, self.on_update_event_info)
        self.box_toolbar.Add(self.button_update_event_info, 1, wx.EXPAND | wx.ALL, 5)

        # Controls toolbar
        self.event_buttons = []
        self.box_controls = wx.BoxSizer(wx.HORIZONTAL)

        self.box.Add(self.box_toolbar, 0, wx.EXPAND)
        self.box.Add(self.event_list, 1, wx.EXPAND)
        self.box.Add(self.box_controls, 0, wx.EXPAND)

        # Events
        self.status_text = "Updating event info..."
        self.Bind(EVT_UPDATE_EVENT_INFO, self.on_update_event_info_done)

        self.Bind(wx.EVT_TIMER, self.on_timer)
        self.timer = wx.Timer(self, -1)
        self.timer.Start(15000)

        # Show
        self.SetSizer(self.box)

    """ Shows the control elements """
    def show_controls(self):
        self.box.Show(self.box_controls)
        self.Layout()

    """ Hides the control elements """
    def hide_controls(self):
        self.box.Hide(self.box_controls)
        self.Layout()

    """ Generates a unique event binding for each button """
    def get_join_event_binding(self, ip):
        return lambda e: self.on_join_event(e, ip)

    """ Returns the selected event """
    def get_selected_event(self):
        item = self.event_list.GetFirstSelected()
        if item != -1:
            event_list = common.event_catalog.get_event_names()
            return event_list[item]

        return None

    """ Opens the event details page in user's default browser """
    def on_double_click_event(self, e):
        event_list = common.event_catalog.get_event_names()
        event = event_list[e.GetIndex()]
        url = common.event_catalog.events[event]["url"]
        webbrowser.open(url)

    """ Updates controls for the currently selected event """
    def on_select_event(self, e):
        self.box_controls.Clear(True)
        self.Layout()

        event = self.get_selected_event()
        if not event:
            return

        button = wx.Button(self, -1, "Select Packs")
        button.Bind(wx.EVT_BUTTON, self.on_select_packs)
        self.box_controls.Add(button, 1, wx.EXPAND | wx.ALL, 5)

        box = wx.BoxSizer(wx.HORIZONTAL)
        self.box_controls.Add(box, 2, wx.EXPAND, 0)

        hosts = common.event_catalog.events[event]["hosts"]
        for host in hosts:
            if host == "TBA":
                button = wx.Button(self, -1, "To be announced")
                button.Disable()
            else:
                ip = hosts[host]
                button = wx.Button(self, -1, f"Join {host} [{ip}]")
                button.Bind(wx.EVT_BUTTON, self.get_join_event_binding(ip))

            box.Add(button, 1, wx.EXPAND | wx.ALL, 5)

        if game_installed():
            self.show_controls()
        else:
            self.hide_controls()

    """ Launches game and connects to the supplied lobby """
    def on_join_event(self, e, ip):
        print_log(f"Joining event {ip}...")
        event = self.get_selected_event()
        recipe = common.event_catalog.recipe_from_event(event)
        self.frame.launch_game(recipe, f"-lobby {ip}")

    """ Prepares a recipe for selecting packs for the event """
    def on_select_packs(self, e):
        event = self.get_selected_event()
        recipe = common.event_catalog.recipe_from_event(event)

        self.frame.game_tab.refresh_presets()
        self.frame.packs_tab.refresh_presets()
        self.frame.packs_tab.set_preset(recipe.slug)

        index = self.parent.FindPage(self.frame.packs_tab)
        self.parent.SetSelection(index)

    """ Refreshes events periodically """
    def on_timer(self, e):
        if not self.IsThisEnabled():
            return

        self.refresh_events()

    """ Updates all event statuses """
    def refresh_events(self):
        status_keys = ["In Progress", "Upcoming"]
        ongoing, upcoming = 0, 0

        for item, event in enumerate(common.event_catalog.get_event_names()):
            status, text = self.get_event_status(common.event_catalog.events[event])
            self.event_list.SetItem(item, 3, f"{status} ({text})")  # status

            if status == status_keys[0]:
                ongoing += 1
            if status == status_keys[1]:
                upcoming += 1

        self.event_list.set_column_widths()

        # Set the events message
        n = ongoing or upcoming
        kind = "ongoing" if ongoing else "upcoming"
        are = "is" if n == 1 else "are"
        s = "" if n == 1 else "s"
        n = f"{n}" if n else "no"
        text = f"There {are} {n} {kind} event{s}."

        self.frame.game_tab.set_events_message(text)

    """ Starts the update thread """
    def on_update_event_info(self, e):
        if not self.IsThisEnabled():
            return

        # Disables the buttons so that it can't be executed again while it's running
        self.frame.disable_events()

        print_log(self.status_text)
        self.frame.set_status(self.status_text)

        self.frame.game_tab.set_events_message(self.status_text)

        worker = UpdateEventInfoThread(self)
        worker.start()

    """ Updates the events list with the gathered information """
    def on_update_event_info_done(self, e):
        pos = self.event_list.get_scroll_pos()
        self.event_list.DeleteAllItems()

        for item, event in enumerate(common.event_catalog.get_event_names()):
            date = common.event_catalog.events[event]["date"]

            self.event_list.InsertItem(item, common.event_catalog.events[event]["title"])
            self.event_list.SetItem(item, 1, f"{date} UTC")  # date
            self.event_list.SetItem(item, 2, self.get_event_host_text(common.event_catalog.events[event]))  # hosts

        self.Layout()
        self.refresh_events()
        self.event_list.set_scroll_pos(pos)

        self.frame.clear_status(self.status_text)
        print_log(self.status_text, "Done!")

        self.on_select_event(None)

        # See if we're connected
        message = "No Internet connection detected."
        self.frame.clear_status(message)
        if not common.event_catalog.is_connected:
            self.frame.set_status(message)

        # Enables the buttons again
        self.frame.enable_events()

    """ Returns the host text for an event from the event catalog """
    def get_event_host_text(self, event):
        text = ""
        n = len(event["hosts"])
        for i, host in enumerate(event["hosts"]):
            if n > 1 and i == n - 1:
                text += " and "
            elif i > 0:
                text += ", "
            text += host

        return text

    """ Returns the event status text """
    def get_event_status(self, event):
        diff = time_diff(event["date"])
        if -7200 < diff < 0:
            return "In Progress", f"for {pretty_time(diff)}"
        elif diff < 0:
            return "Ended", f"{pretty_time(diff + 7200)} ago"
        else:
            return "Upcoming", f"{pretty_time(diff)} from now"


class RepositoryTab(TabPage):
    def __init__(self, parent):
        TabPage.__init__(self, parent)
        self.init_ui()

    def init_ui(self):
        self.box = wx.BoxSizer(wx.VERTICAL)

        # Repository List
        widths = [320, 180, 100, 160]
        self.list = CheckListCtrl(self, widths, -1, style=wx.LC_REPORT)
        self.list.InsertColumn(0, "URL", width=widths[0])
        self.list.InsertColumn(1, "Name", width=widths[1])
        self.list.InsertColumn(2, "Version", width=widths[2])
        self.list.InsertColumn(3, "Status", width=widths[3])

        self.list.Bind(EVT_LIST_ITEM_CHECKED, self.on_toggled)
        self.list.Bind(EVT_LIST_ITEM_UNCHECKED, self.on_toggled)

        # Main toolbar
        self.box_toolbar = wx.BoxSizer(wx.HORIZONTAL)
        self.button_update_info = wx.Button(self, -1, "Update Repository Info")
        self.button_update_info.Bind(wx.EVT_BUTTON, self.on_update_repo_info)
        self.box_toolbar.Add(self.button_update_info, 1, wx.EXPAND | wx.ALL, 5)

        # Controls toolbar
        self.box_controls = wx.BoxSizer(wx.HORIZONTAL)
        self.repo_add_text = wx.TextCtrl(self)
        self.repo_add_text.SetMinSize(wx.Size(300, -1))
        self.repo_add_button = wx.Button(self, label="Add Repository")
        self.repo_add_button.SetMinSize(wx.Size(150, -1))
        self.repo_add_button.Bind(wx.EVT_BUTTON, self.on_repo_add)
        self.repo_remove_button = wx.Button(self, label="Remove")
        self.repo_remove_button.SetMinSize(wx.Size(150, -1))
        self.repo_remove_button.Bind(wx.EVT_BUTTON, self.on_repo_remove)

        self.box_controls.Add(self.repo_add_text, 1, wx.EXPAND | wx.ALL, 5)
        self.box_controls.Add(self.repo_add_button, 0, wx.EXPAND | wx.ALL, 5)
        self.box_controls.Add(self.repo_remove_button, 0, wx.EXPAND | wx.ALL, 5)

        self.box.Add(self.box_toolbar, 0, wx.EXPAND)
        self.box.Add(self.list, 1, wx.EXPAND)
        self.box.Add(self.box_controls, 0, wx.EXPAND)

        self.list.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.on_double_click)
        self.list.Bind(wx.EVT_LIST_ITEM_SELECTED, self.on_selected)
        self.list.Bind(wx.EVT_LIST_ITEM_DESELECTED, self.on_selected)

        self.SetSizer(self.box)

    """ Add the repo from the text field to the repo dict """
    def on_repo_add(self, e):
        repo = self.repo_add_text.GetValue()
        if not repo:
            return

        self.repo_add_text.Clear()  # Empty TextCtrl after adding a new repo

        common.repo.add_local_repo(repo)
        self.refresh_repos()

    """ Removes the selected repo from the repo list and the dict """
    def on_repo_remove(self, e):
        repo_list = self.get_selected_repos()
        if repo_list:
            common.repo.remove_local_repos(repo_list)
            self.refresh_repos()

    """ A repo is activated or deactivated """
    def on_toggled(self, e):
        repo = self.list.GetItemText(e.item)  # with ListCtrl GetIndex() instead of item
        common.repo.enable_repo(repo, e.value)
        self.refresh_repos()

    """ A repo is selected or deselected """
    def on_selected(self, e):
        enabled = bool(self.get_selected_repos())
        self.repo_remove_button.Enable(enabled)

    """ A repo is double clicked """
    def on_double_click(self, e):
        repo_list = self.get_selected_repos()
        if repo_list:
            url = repo_list[0]
            webbrowser.open(url)

    """ Starts the update info thread """
    def on_update_repo_info(self, e):
        if not self.IsThisEnabled():
            return

        self.frame.refresh_packs()
        self.frame.refresh_events()

    """ Updates the repositories list """
    def refresh_repos(self):
        pos = self.list.get_scroll_pos()
        self.list.DeleteAllItems()

        for item, repo in enumerate(common.repo.repo_info):  # List all repos
            ver = common.repo.repo_info[repo]["version"]

            self.list.InsertItem(item, repo)
            self.list.SetItem(item, 1, common.repo.repo_info[repo]["name"])
            self.list.SetItem(item, 2, f"{ver:.4f}" if ver else "N/A")
            self.list.SetItem(item, 3, common.repo.repo_info[repo]["status"])

            enabled = not common.repo.repo_info[repo]["disabled"]
            self.list.CheckItem(item, check=enabled, event=False)

        self.Layout()
        self.list.set_column_widths()
        self.list.set_scroll_pos(pos)

        self.on_selected(None)

    """ Returns the list of selected repositories """
    def get_selected_repos(self):
        repo_list = []  # Allows multiple selections
        item = self.list.GetFirstSelected()
        while item != -1:
            repo = self.list.GetItemText(item)
            item = self.list.GetNextSelected(item)
            repo_list.append(repo)

        return repo_list

    """ Enables or disables the pane """
    def set_state(self):
        tabs = [self.frame.packs_tab, self.frame.events_tab]
        enable = all([t.IsThisEnabled() for t in tabs])
        self.Enable(enable)


class PacksTab(TabPage):
    def __init__(self, parent):
        TabPage.__init__(self, parent)
        self.init_ui()

    def init_ui(self):
        self.box = wx.BoxSizer(wx.VERTICAL)

        # Package list
        widths = [225, 100, 275, 160]
        self.pack_list = CheckListCtrl(self, widths, -1, style=wx.LC_REPORT)
        self.pack_list.InsertColumn(0, "Pack", width=widths[0])
        self.pack_list.InsertColumn(1, "Version", width=widths[1])
        self.pack_list.InsertColumn(2, "Description", width=widths[2])
        self.pack_list.InsertColumn(3, "Status", width=widths[3])

        self.pack_list.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.on_double_click_pack)
        self.pack_list.Bind(EVT_LIST_ITEM_CHECKED, self.on_toggle_pack)
        self.pack_list.Bind(EVT_LIST_ITEM_UNCHECKED, self.on_toggle_pack)

        # Main toolbar
        self.box_toolbar = wx.BoxSizer(wx.HORIZONTAL)
        self.button_update_pack_info = wx.Button(self, -1, "Update Pack Info")
        self.button_update_pack_info.Bind(wx.EVT_BUTTON, self.on_update_pack_info)
        self.box_toolbar.Add(self.button_update_pack_info, 1, wx.EXPAND | wx.ALL, 5)

        # Controls toolbar
        self.box_controls = wx.BoxSizer(wx.HORIZONTAL)
        self.recipes = [s for s, r in common.recipes.items() if r.local]
        self.choice_preset = wx.Choice(self, -1, choices=self.recipes)
        self.choice_preset.Bind(wx.EVT_CHOICE, self.on_preset_selection)
        self.choice_preset.SetMinSize(wx.Size(160, -1))
        self.button_add = wx.Button(self, -1, "New Preset")
        self.button_add.SetMinSize(wx.Size(100, -1))
        self.button_add.Bind(wx.EVT_BUTTON, self.on_new_preset)
        self.button_remove = wx.Button(self, -1, "Delete Preset")
        self.button_remove.SetMinSize(wx.Size(100, -1))
        self.button_remove.Bind(wx.EVT_BUTTON, self.on_delete_preset)
        self.button_update = wx.Button(self, -1, "Install Updates")
        self.button_update.SetMinSize(wx.Size(160, -1))
        self.button_update.Bind(wx.EVT_BUTTON, self.on_update_game)

        self.box_controls.Add(self.choice_preset, 1, wx.EXPAND | wx.ALL, 5)
        self.box_controls.Add(self.button_add, 1, wx.EXPAND | wx.ALL, 5)
        self.box_controls.Add(self.button_remove, 1, wx.EXPAND | wx.ALL, 5)
        self.box_controls.Add(self.button_update, 1, wx.EXPAND | wx.ALL, 5)

        # Preset toolbar
        self.box_preset = wx.BoxSizer(wx.HORIZONTAL)
        base_recipes = [s for s, r in common.recipes.items() if r.install]
        choices = [common.recipes[r].title for r in base_recipes]
        self.text_preset_base = wx.StaticText(self, -1, "Base Preset:")
        self.choice_preset_base = wx.Choice(self, -1, choices=choices)
        self.text_preset_name = wx.StaticText(self, -1, "Preset Name:")
        self.ctrl_preset_name = wx.TextCtrl(self)
        self.ctrl_preset_name.Bind(wx.EVT_TEXT, self.on_preset_name_enter)
        self.button_preset_add = wx.Button(self, -1, "Add")
        self.button_preset_add.SetDefault()
        self.button_preset_add.Bind(wx.EVT_BUTTON, self.on_add_preset)
        self.button_preset_cancel = wx.Button(self, -1, "Cancel")
        self.button_preset_cancel.Bind(wx.EVT_BUTTON, self.on_cancel_preset)

        self.box_preset.Add(self.text_preset_base, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)
        self.box_preset.Add(self.choice_preset_base, 1, wx.EXPAND | wx.ALL, 5)
        self.box_preset.Add(self.text_preset_name, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)
        self.box_preset.Add(self.ctrl_preset_name, 1, wx.EXPAND | wx.ALL, 5)
        self.box_preset.Add(self.button_preset_add, 0, wx.EXPAND | wx.ALL, 5)
        self.box_preset.Add(self.button_preset_cancel, 0, wx.EXPAND | wx.ALL, 5)

        # Install toolbar
        self.box_install = wx.BoxSizer(wx.HORIZONTAL)
        self.button_install = wx.Button(self, -1, "Add Packs and Install")
        self.button_install.Bind(wx.EVT_BUTTON, self.on_add_packs)
        self.box_install.Add(self.button_install, 1, wx.EXPAND | wx.ALL, 5)

        self.box.Add(self.box_toolbar, 0, wx.EXPAND)
        self.box.Add(self.pack_list, 1, wx.EXPAND)
        self.box.Add(self.box_controls, 0, wx.EXPAND)
        self.box.Add(self.box_preset, 0, wx.EXPAND)
        self.box.Add(self.box_install, 0, wx.EXPAND)

        # Events
        self.status_text = "Updating pack info..."
        self.Bind(EVT_UPDATE_PACK_INFO, self.on_update_pack_info_done)
        self.Bind(EVT_UPDATE_GAME, self.on_update_game_done)
        self.Bind(EVT_CLEANUP_GAME, self.on_cleanup_game_done)

        # Show
        self.set_preset(CONFIG["default-recipe"])

        if game_installed():
            self.show_controls()
        else:
            self.show_install()

        self.SetSizer(self.box)

    """ Hides installer elements and shows the control elements """
    def show_controls(self):
        self.box.Hide(self.box_install)
        self.box.Hide(self.box_preset)
        self.box.Show(self.box_controls)
        self.Layout()

    """ Hides control elements and shows the preset elements """
    def show_preset_controls(self):
        self.box.Hide(self.box_install)
        self.box.Hide(self.box_controls)
        self.box.Show(self.box_preset)
        self.Layout()

    """ Hides control elements and shows the installer elements """
    def show_install(self):
        self.box.Hide(self.box_controls)
        self.box.Hide(self.box_preset)
        self.box.Show(self.box_install)
        self.Layout()

    """ Returns the list of selected packages """
    def get_selected_packs_list(self):
        plist = []
        item = self.pack_list.GetFirstSelected()
        while item != -1:
            pack = self.pack_list.GetItemText(item)
            item = self.pack_list.GetNextSelected(item)
            plist.append(pack)

        return plist

    """ Returns the selected preset slug """
    def get_selected_preset(self):
        if game_installed():
            return self.choice_preset.GetStringSelection()
        else:
            return "custom"

    """ Functions for managing presets """
    def set_preset(self, recipe):
        recipe = ("default", recipe)[recipe in self.recipes]
        if recipe in self.recipes:
            self.choice_preset.SetSelection(self.recipes.index(recipe))
            self.on_preset_selection(None)

    def refresh_presets(self):
        self.recipes = [s for s, r in common.recipes.items() if r.local]
        self.choice_preset.Clear()
        self.choice_preset.Append(self.recipes)
        self.set_preset(CONFIG["default-recipe"])

    """ Opens a package's installation folder """
    def open_pack_dir(self):
        plist = self.get_selected_packs_list()
        if plist and common.repo.is_installed(plist[0]):
            open_folder(common.repo.get_package_dir(plist[0]))
        else:
            open_folder(os.path.join(get_data_dir(), "packs"))

    """ Updates all package statuses """
    def refresh_packs(self):
        status_keys = ["Pending Install", "Update Available"]
        install, update = 0, 0
        rvgl_version = 0

        recipe = common.recipes.get(self.get_selected_preset(), None)
        for item, pack in enumerate(common.repo.package_info):
            status = self.get_pack_status(pack)
            self.pack_list.SetItem(item, 3, status)

            enabled = bool(recipe and recipe.has_package(pack))
            self.pack_list.CheckItem(item, check=enabled, event=False)

            if status == status_keys[0]:
                install += 1
            if status == status_keys[1]:
                update += 1

            if status in status_keys and common.recipes["update"].has_package(pack):
                ver = common.repo.package_info[pack]["version"]
                rvgl_version = max(rvgl_version, ver)

        self.pack_list.set_column_widths()

        # Set the packs message
        n = install or update
        need = "needs" if n == 1 else "need"
        s = "" if n == 1 else "s"

        if install:
            text = f"{n} package{s} {need} to be installed."
            label = "Install Packages"
        elif update:
            text = f"{n} package{s} can be updated."
            label = "Update Packages"
        else:
            text = "All packages are up to date."
            label = "No Updates Available"

        self.frame.game_tab.set_packs_message(text)

        self.button_update.SetLabel(label)
        self.button_update.Enable(bool(n))

        # Set the update message
        if rvgl_version:
            text = f"RVGL {rvgl_version:.4f}a is available."
        else:
            text = "RVGL is up to date."

        self.frame.game_tab.set_rvgl_message(text)
        self.frame.game_tab.rvgl_button.Enable(bool(rvgl_version))

        enable = bool(recipe and recipe.slug != "default")
        self.button_remove.Enable(enable)

        self.frame.game_tab.on_preset_selection(None)

        # Show or hide controls
        if game_installed():
            self.frame.game_tab.show_launch()
            self.frame.events_tab.show_controls()
            self.show_controls()
        else:
            self.frame.game_tab.show_install()
            self.frame.events_tab.hide_controls()
            self.show_install()

    """ Event handlers for managing presets """
    def on_preset_selection(self, e):
        self.refresh_packs()

    def on_toggle_pack(self, e):
        recipe = common.recipes.get(self.get_selected_preset(), None)
        if not all([recipe, self.IsEnabled()]):
            return

        pack = self.pack_list.GetItemText(e.item)
        if e.value and not recipe.has_package(pack):
            recipe.add_packages([pack])
        elif not e.value and recipe.has_package(pack):
            recipe.remove_packages([pack])
        else:
            return

        recipe.save()
        self.refresh_packs()

    def on_new_preset(self, e):
        self.show_preset_controls()
        self.choice_preset_base.SetSelection(0)
        self.ctrl_preset_name.Clear()
        self.ctrl_preset_name.SetFocus()

    def on_delete_preset(self, e):
        recipe = common.recipes.get(self.get_selected_preset(), None)
        if not all([recipe, self.IsEnabled()]):
            return

        if not recipe.local or recipe.slug == "default":
            return

        recipe.remove()
        common.recipes.pop(recipe.slug)

        self.frame.game_tab.refresh_presets()
        self.refresh_presets()

    def on_preset_name_enter(self, e):
        name = slugify(self.ctrl_preset_name.GetValue())
        enable = bool(name and name not in common.recipes)
        self.button_preset_add.Enable(enable)

    def on_add_preset(self, e):
        base = slugify(self.choice_preset_base.GetStringSelection())
        name = slugify(self.ctrl_preset_name.GetValue())
        if not name or name in common.recipes:
            return

        packages = common.recipes[base].packages.copy()
        common.recipes[name] = Recipe(packages, name, local=True)
        common.recipes[name].save()

        self.frame.game_tab.refresh_presets()
        self.refresh_presets()
        self.set_preset(name)

    def on_cancel_preset(self, e):
        self.show_controls()

    """ Handles a package double click event """
    def on_double_click_pack(self, e):
        self.open_pack_dir()

    """ Starts the update info thread """
    def on_update_pack_info(self, e, fetch=True, local=False):
        if not self.IsThisEnabled():
            return

        if not fetch or local:
            common.repo.update_package_info(fetch=False, local=local)
            self.on_update_pack_info_done(None, fetch=False)
            self.frame.local_tab.on_reload_local_files(None)
            return

        # Disables the buttons so that it can't be executed again while it's running
        self.frame.disable_packs()

        self.status_text = "Updating pack info..."

        print_log(self.status_text)
        self.frame.set_status(self.status_text)

        self.frame.game_tab.set_packs_message(self.status_text)

        worker = UpdatePackInfoThread(self)
        worker.start()

    """ Updates the package list with the gathered information """
    def on_update_pack_info_done(self, e, fetch=True):
        pos = self.pack_list.get_scroll_pos()
        self.pack_list.DeleteAllItems()

        for item, pack in enumerate(common.repo.package_info):
            ver = common.repo.package_info[pack]["version"]

            self.pack_list.InsertItem(item, pack)
            self.pack_list.SetItem(item, 1, f"{ver:.4f}" if ver else "N/A")
            self.pack_list.SetItem(item, 2, common.repo.package_info[pack]["description"])

        self.Layout()
        self.refresh_packs()
        self.pack_list.set_scroll_pos(pos)

        if not fetch:
            return

        self.frame.repository_tab.refresh_repos()

        self.frame.clear_status(self.status_text)
        print_log(self.status_text, "Done!")

        # See if we're connected
        message = "No Internet connection detected."
        self.frame.clear_status(message)
        if not common.repo.is_connected:
            self.frame.set_status(message)

        # Enables the buttons again
        self.frame.enable_packs()

    """ Starts the add packs thread """
    def on_add_packs(self, e):
        recipe = common.recipes.get(self.get_selected_preset(), None)
        if not all([recipe, self.IsEnabled()]):
            return

        plist = self.get_selected_packs_list()
        recipe.add_packages(plist)

        if not any([recipe.packages, game_installed()]):
            message = "\n".join(("You haven't selected any content packs!",
                "This will install platform binaries and nothing else.", "Continue?"))
            if self.frame.show_message("Warning", message) != wx.ID_YES:
                return

        self.on_update_game(None)

    """ Starts the remove packs thread """
    def on_remove_packs(self, e):
        recipe = common.recipes.get(self.get_selected_preset(), None)
        if not all([recipe, self.IsEnabled()]):
            return

        plist = self.get_selected_packs_list()
        recipe.remove_packages(plist)

        self.on_update_game(None)

    """ Starts the update game thread """
    def on_update_game(self, e, clean=False, uninstall=False, recipes=[]):
        recipe = common.recipes.get(self.get_selected_preset(), None)
        if not all([recipe, self.IsEnabled()]):
            return

        if not game_installed():
            self.frame.game_tab.set_install_preset(recipe.slug)
            self.frame.game_tab.on_download_and_install(None)
            return

        if self.frame.get_worker():
            print_log("Worker thread is currently busy!")
            return

        recipes = recipes or [common.recipes[r] for r in self.recipes]

        # Disables the buttons so that it can't be executed again while it's running
        self.frame.disable_game()
        self.frame.disable_packs()

        if uninstall:
            self.status_text = "Removing packages..."
        elif clean:
            self.status_text = "Repairing packages..."
        else:
            self.status_text = "Updating packages..."

        print_log(self.status_text)
        self.frame.set_status(self.status_text)

        self.frame.game_tab.set_packs_message(self.status_text)

        worker = UpdateGameThread(self, clean, uninstall, recipes)
        self.frame.set_worker(worker)
        worker.start()

    """ Responds to the end of update game thread """
    def on_update_game_done(self, e):
        self.frame.clear_status(self.status_text)
        print_log(self.status_text, "Done!")

        self.frame.set_worker(None)

        # Enables the buttons again
        self.frame.enable_game()
        self.frame.enable_packs()
        self.refresh_packs()

    """ Starts the cleanup game thread """
    def on_cleanup_game(self, e):
        if not self.IsEnabled():
            return

        if self.frame.get_worker():
            print_log("Worker thread is currently busy!")
            return

        packs, dloads, logs = False, False, False
        if self.frame.show_message("Clean Up", "Clear unused packages?") == wx.ID_YES:
            packs = True
        if self.frame.show_message("Clean Up", "Clear downloaded files?") == wx.ID_YES:
            dloads = True
        if self.frame.show_message("Clean Up", "Clear log files?") == wx.ID_YES:
            logs = True

        if not any([packs, dloads, logs]):
            return

        # Disables the buttons so that it can't be executed again while it's running
        self.frame.disable_packs()

        self.status_text = "Cleaning up packages..."

        print_log(self.status_text)
        self.frame.set_status(self.status_text)

        self.frame.game_tab.set_packs_message(self.status_text)

        worker = CleanupGameThread(self, packs, dloads, logs)
        self.frame.set_worker(worker)
        worker.start()

    """ Responds to the end of cleanup game thread """
    def on_cleanup_game_done(self, e):
        self.frame.clear_status(self.status_text)
        print_log(self.status_text, "Done!")

        self.frame.set_worker(None)

        # Enables the buttons again
        self.frame.enable_packs()
        self.refresh_packs()

    """ Removes unused package files """
    def clean_packs(self):
        for pack in common.repo.package_info:
            if not common.repo.is_installed(pack):
                continue

            if not self.is_pack_enabled(pack):
                common.repo.remove_package(pack)

    """ Returns whether package is enabled in any preset """
    def is_pack_enabled(self, pack, selected=False):
        if selected or not game_installed():
            recipe = common.recipes.get(self.get_selected_preset(), None)
            return bool(recipe and recipe.has_package(pack))

        for recipe in self.recipes:
            if common.recipes[recipe].has_package(pack):
                return True

        return False

    """ Returns the package status text """
    def get_pack_status(self, pack):
        if common.repo.is_platform_package(pack):
            if pack != f"rvgl_{PLATFORM}":
                return "Not Applicable"

        installed = common.repo.is_installed(pack)
        outdated = common.repo.is_outdated(pack)
        enabled = self.is_pack_enabled(pack)

        if installed and outdated:
            return "Update Available"
        elif installed:
            return "Installed"
        elif enabled:
            return "Pending Install"
        else:
            return "Not Installed"


class LocalTab(TabPage):
    def __init__(self, parent):
        TabPage.__init__(self, parent)
        self.init_ui()

    def init_ui(self):
        self.box = wx.BoxSizer(wx.VERTICAL)

        # Main toolbar
        self.box_toolbar = wx.BoxSizer(wx.HORIZONTAL)
        self.button_update_info = wx.Button(self, -1, "Reload Local Files")
        self.button_update_info.Bind(wx.EVT_BUTTON, self.on_reload_local_files)
        self.box_toolbar.Add(self.button_update_info, 1, wx.EXPAND | wx.ALL, 5)

        # Files list
        self.list = wx.ListCtrl(self, -1, style=wx.LC_LIST)
        self.list.SetMinSize(wx.Size(300, 50))
        self.list.SetSize(wx.Size(300, 50))

        self.list.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.on_double_click)

        # Controls toolbar
        self.box_controls = wx.BoxSizer(wx.HORIZONTAL)

        self.types = common.repo.get_content_types() # all types which could be added via a zip file
        self.choice_type = wx.Choice(self, -1, choices=self.types)
        self.choice_type.SetSelection(0)
        self.choice_type.Bind(wx.EVT_CHOICE, self.on_type_selection)
        self.box_controls.Add(self.choice_type, 2, wx.EXPAND | wx.ALL, 5)

        self.remove_button = wx.Button(self, label="Remove Selected")
        self.remove_button.SetMinSize(wx.Size(150, -1))
        self.remove_button.Bind(wx.EVT_BUTTON, self.on_remove)
        self.box_controls.Add(self.remove_button, 1, wx.EXPAND | wx.ALL, 5)

        # File drop toolbar
        self.box_file_drop = wx.BoxSizer(wx.VERTICAL)

        self.file_drop_target = FileDropTarget(self)
        self.drag_and_drop_info = wx.StaticText(self, label="Drag archives here and they'll be installed to your local pack (*.zip and *.7z are supported).")
        self.file_text_ctrl = wx.TextCtrl(self, style=wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL)
        self.file_text_ctrl.SetMinSize(wx.Size(300, 150))
        self.file_text_ctrl.SetDropTarget(self.file_drop_target)

        self.box_file_drop.Add(self.drag_and_drop_info, 0, wx.ALL, 5)
        self.box_file_drop.Add(self.file_text_ctrl, 1, wx.EXPAND | wx.ALL, 5)

        # Add all toolbars to the box
        self.box.Add(self.box_toolbar, 0, wx.EXPAND)
        self.box.Add(self.list, 1, wx.EXPAND | wx.ALL, 5)
        self.box.Add(self.box_controls, 0, wx.EXPAND)
        self.box.Add(self.box_file_drop, 0, wx.EXPAND)

        # Events
        self.status_text = "Installing local content..."
        self.Bind(EVT_DROP_FILES, self.on_drop_files)
        self.Bind(EVT_INSTALL_LOCAL, self.on_install_local_done)

        self.Bind(wx.EVT_TIMER, self.on_timer)
        self.timer = wx.Timer(self, -1)
        self.timer.Start(1000)

        # Show
        self.on_type_selection(None)
        self.SetSizer(self.box)

    """ Updates the text of the Drag-and-Drop TextView """
    def update_text(self, text):
        self.file_text_ctrl.WriteText(text)

    """ Returns the list of files and folder in the supplied path """
    def get_files_list(self, path):
        try:
            entries = next(os.walk(path))
            entries[1].sort() # folders first in sorted order
            entries[2].sort() # then files in sorted order
            return entries[1] + entries[2]
        except Exception as e:
            return []

    """ Returns the list of selected files and folders """
    def get_selected_files(self):
        selected_list = []  # Allows multiple selections
        item = self.list.GetFirstSelected()
        while item != -1:
            selected = self.list.GetItemText(item)
            item = self.list.GetNextSelected(item)
            selected_list.append(selected)

        return selected_list

    """ Returns whether the file should be displayed in the files list """
    def filter_file(self, selected_type, entry):
        if entry.startswith("."):
            return False

        if selected_type == "gfx":
            name = os.path.splitext(entry)[0]
            local_path = common.repo.get_package_dir("local")
            level_path = os.path.join(local_path, "levels", name)
            return not os.path.isdir(level_path)

        return True

    """ Periodically check the download queue """
    def on_timer(self, e):
        if self.frame.url_queue.empty():
            return

        if not self.IsEnabled():
            return

        if self.frame.get_worker():
            print_log("Worker thread is currently busy!")
            return

        files = {}
        while not self.frame.url_queue.empty():
            filename, url = self.frame.url_queue.get()
            self.update_text(f"Queueing file for download: {filename}\n")
            files[filename] = url

        self.on_install_local(None, files, download=True)

        index = self.parent.FindPage(self)
        self.parent.SetSelection(index)

    """ Reload the list """
    def on_reload_local_files(self, e):
        self.on_type_selection(None)

    """ Handles double click on a content folder """
    def on_double_click(self, e):
        selected_type = self.types[self.choice_type.GetSelection()]
        local_path = common.repo.get_package_dir("local")
        path = os.path.join(local_path, selected_type)

        selected = self.get_selected_files()
        if not selected:
            return

        selected_path = os.path.join(path, selected[0])
        if os.path.isdir(selected_path):
            open_folder(selected_path)
        else:
            open_folder(path)

    """ Remove button triggered """
    def on_remove(self, e):
        selected_type = self.types[self.choice_type.GetSelection()]
        local_path = common.repo.get_package_dir("local")
        path = os.path.join(local_path, selected_type)

        if selected_type == "levels":
            gfx_path = os.path.join(local_path, "gfx")
            gfx_files_list = self.get_files_list(gfx_path)

        remove_paths = []
        for selected in self.get_selected_files():
            remove_paths.append(os.path.join(path, selected))
            # if this is a level, remove associated gfx files
            if selected_type == "levels" and os.path.isdir(remove_paths[-1]):
                for entry in gfx_files_list:
                    if os.path.splitext(entry)[0] == selected:
                        remove_paths.append(os.path.join(gfx_path, entry))

        for path in remove_paths:
            if os.path.isdir(path): # if its a folder
                remove_folder(path)
                continue
            try: # or a file
                os.remove(path)
            except Exception as e:
                continue

        self.on_type_selection(None)  # Refresh the list

    """ Selection of an item type of the local folder to manage """
    def on_type_selection(self, e):
        selected_type = self.types[self.choice_type.GetSelection()]
        local_path = common.repo.get_package_dir("local")
        path = os.path.join(local_path, selected_type)

        self.list.DeleteAllItems()
        if os.path.isdir(path):
            for entry in self.get_files_list(path):
                if self.filter_file(selected_type, entry):
                    self.list.Append([entry])

    """ Process dropped files and extract them to the local pack """
    def on_drop_files(self, e):
        files = {f: "" for f in e.filenames}
        self.on_install_local(None, files)

    """ Starts the local content download thread """
    def on_install_local(self, e, files, download=False):
        if not self.IsEnabled():
            return

        if self.frame.get_worker():
            print_log("Worker thread is currently busy!")
            return

        # Disables the buttons so that it can't be executed again while it's running
        self.frame.disable_game()
        self.frame.disable_packs()

        print_log(self.status_text)
        self.frame.set_status(self.status_text)

        worker = InstallLocalThread(self, files, download)
        self.frame.set_worker(worker)
        worker.start()

    """ Responds to the end of local content installation """
    def on_install_local_done(self, e):
        if e.value:
            path, success = e.value
            success = ("Failed", "Success")[success]
            self.update_text(f"Unpacking file '{path}': {success}\n")
            return

        self.frame.clear_status(self.status_text)
        print_log(self.status_text, "Done!")

        self.frame.set_worker(None)

        # Enables the buttons again
        self.frame.enable_game()
        self.frame.enable_packs()

        self.on_type_selection(None)


class ConsoleTab(TabPage):
    def __init__(self, parent):
        TabPage.__init__(self, parent)
        self.init_ui()

    def init_ui(self):
        self.box = wx.BoxSizer(wx.VERTICAL)

        self.text_console = wx.TextCtrl(self, -1, style=wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL)
        self.box.Add(self.text_console, 1, wx.EXPAND | wx.ALL, 5)

        self.Bind(wx.EVT_TIMER, self.on_timer)
        self.timer = wx.Timer(self, -1)
        self.timer.Start(1000)

        self.SetSizer(self.box)

    def on_timer(self, e):
        LogFile.print_ctrl(self.text_console)

