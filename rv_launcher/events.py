import requests
from urllib.parse import urljoin
from rv_launcher import common
from rv_launcher.common import *
from rv_launcher.helpers import *
from rv_launcher.recipes import Recipe


class EventCatalog:

    def __init__(self):
        self.events = {}
        self.is_connected = True

    """ Gets event information from available sources """
    def download_events(self):
        try:
            self.is_connected = True
            r = session.get(RVIO_EVENTS_URL, timeout=15)
            r.raise_for_status()
            event_list = r.json()
        except Exception as e:
            print_log(f"Could not get event information.\n  {e}")
            if isinstance(e, requests.ConnectionError):
                self.is_connected = False
            return False

        self.events.clear()

        # Read the default list of events
        for event in event_list:
            event_url = urljoin(RVIO_URL, f"{event_list[event]['url']}")
            print_log(f"Adding event: {event_url}")
            try:
                url = f"{event_url}?return-as=json"
                r = session.get(url, timeout=15)
                r.raise_for_status()
                event_json = r.json()

                self.events[event] = {}
                self.events[event]["title"] = event_json["header"]["title"]
                self.events[event]["date"] = event_json["header"]["date"]
                self.events[event]["packages"] = event_json["header"]["event"].get("packages", [])
                self.events[event]["hosts"] = event_json["header"]["event"].get("hosts", {})
                self.events[event]["url"] = event_url
                self.events[event]["source"] = "Re-Volt I/O"
                self.events[event]["tracklist"] = event_json["header"]["event"].get("tracklist", "").split("\n")  # Tracklist is a string and splitted to a list of tracks
                self.events[event]["class"] = event_json["header"]["event"].get("class", "")
            except Exception as e:
                print_log(f"Could not get information for event {event}.\n  {e}")
                self.events.pop(event, None)
                continue

            # Process legacy event format (shouldn't be used anymore)
            if not self.events[event]["hosts"]:
                host = event_json["header"]["event"].get("host", "")
                ip = event_json["header"]["event"].get("ip", "")
                if host:
                    self.events[event]["hosts"][host] = ip

                host = event_json["header"]["event"].get("host2", "")
                ip = event_json["header"]["event"].get("ip2", "")
                if host:
                    self.events[event]["hosts"][host] = ip

        # Read events from repositories
        with common.repo.lock:
            event_id = 0
            for repo in common.repo.repo_info:
                event_list = common.repo.repo_info[repo]["events"]
                for event in event_list:
                    event_slug = f"{event}-{event_id:04}"
                    event_id += 1

                    print_log(f"Adding event: {event_slug}")

                    self.events[event_slug] = {}
                    self.events[event_slug]["title"] = event_list[event].get("title", "N/A")
                    self.events[event_slug]["date"] = event_list[event].get("date", "N/A")
                    self.events[event_slug]["packages"] = event_list[event].get("packages", [])
                    self.events[event_slug]["hosts"] = event_list[event].get("hosts", {})
                    self.events[event_slug]["url"] = event_list[event].get("url", repo)  # default url is url of the json file
                    self.events[event_slug]["source"] = common.repo.repo_info[repo]["name"]
                    self.events[event_slug]["tracklist"] = event_list[event].get("tracklist", [])
                    self.events[event_slug]["class"] = event_list[event].get("class", "")

        return True

    """ Returns a list of all available event names """
    def get_event_names(self):
        return list(self.events.keys())

    """ Returns a recipe from any given event name"""
    def recipe_from_event(self, event):
        default = common.recipes["default"]
        if not event or event not in self.events:
            return default

        slug = slugify(event)
        if slug in common.recipes:
            return common.recipes[slug]

        packages = default.packages.copy()
        recipe = Recipe(packages, event, local=True, writable=False, root=default.root)
        recipe.add_packages(self.events[event]["packages"])
        recipe.save()

        common.recipes[slug] = recipe
        return recipe
