import os
import glob
from rv_launcher.common import *
from rv_launcher.helpers import *
from rv_launcher.recipes import Recipe


""" Makes sure all folders exist """
def prepare_folders():
    # Required in case we were launched by an URI handler
    try:
        os.chdir(get_app_path())
    except Exception as e:
        print_log(f"Could not change to app folder.\n  {e}")

    for folder in ["downloads", "packs"]:
        create_folder(os.path.join(get_data_dir(), folder))
        create_folder(os.path.join(get_data_dir(), folder, "local"))

    for folder in ["downloads", "update"]:
        create_folder(os.path.join(DATA_DIR, folder))

    for folder in ["logs", "recipes", "repos"]:
        create_folder(os.path.join(CONFIG_DIR, folder))

    create_folder(get_save_dir())

    # See if the URI was registered previously
    if not CONFIG["uri-registered"]:
        register_uri()


""" Prepares all preset recipes so the game can be installed. """
def prepare_recipes():
    # Set up install presets
    recipes["original"] = Recipe(["game_files", "rvgl_assets", "rvgl_dcpack", "soundtrack"],
        "Original", "Includes the original soundtrack.", install=True)
    recipes["original"].save()

    recipes["online"] = Recipe(["game_files", "rvgl_assets", "rvgl_dcpack",
        "io_tracks", "io_tracks_bonus", "io_tracks_circuit",
        "io_cars", "io_cars_bonus", "io_skins", "io_skins_bonus",
        "io_music", "io_soundtrack", "io_lmstag", "io_loadlevel",
        "io_clockworks", "io_clockworks_modern"], "Online",
        "Includes community soundtrack and additional content for playing online.", install=True)
    recipes["online"].save()

    recipes["basic"] = Recipe(["game_files", "rvgl_assets", "rvgl_dcpack"],
        "Basic", "Does not include the soundtrack.", install=True)
    recipes["basic"].save()

    recipes["custom"] = Recipe(["rvgl_assets"],
        "Custom", "Choose your own packs...", install=True, root=True)
    recipes["custom"].save()

    recipes["update"] = Recipe(["rvgl_assets"], "Update")
    recipes["update"].save()

    # Set up local presets
    recipes["default"] = Recipe([], "Default", local=True)
    recipes["default"].load()

    recipe_files = os.path.join(CONFIG_DIR, "recipes", "*.json")
    for path in glob.glob(recipe_files):
        name, _ = os.path.splitext(os.path.basename(path))
        if name == slugify(name) and name not in recipes:
            recipes[name] = Recipe([], name, local=True)
            recipes[name].load()

