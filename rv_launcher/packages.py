import os
import json
import threading
import requests
from urllib.parse import urlparse
from rv_launcher import common
from rv_launcher.common import *
from rv_launcher.helpers import *
from rv_launcher.config import *


""" Convert a version string to float """
def get_float_version(ver):
    try:
        return float(ver)
    except Exception:
        return 0.0


""" Gets the current version of RVGL from the website """
def get_rvgl_version():
    try:
        r = session.get(RVGL_VERSION_URL, timeout=15)
        r.raise_for_status()
        data = r.content
        return float(data.decode("utf-8").strip())
    except Exception as e:
        print_log(f"Could not get RVGL version.\n  {e}")
        return 0.0


class Repo:

    def __init__(self):
        self.special_packages = {
            "default": "Content present in the root game folder.",
            "local": "Locally available content (writable)."
        }
        self.package_info = {}
        self.repo_info = {}
        self.lock = threading.Lock()
        self.is_connected = True


    """ Generates a dictionary with packages, versions and URLs """
    def generate_package_info(self, fetch=True):
        with self.lock:
            self.package_info.clear()
            self.repo_info.clear()

            for package, description in self.special_packages.items():
                self.add_package(package, description=description, local=True)

            plist = self.get_package_list(fetch=fetch)
            for package, pinfo in plist.items():
                url = f"{PACKAGE_URL}{package}.zip"
                self.add_package(package, pinfo, url=url)

            self.add_custom_repos(fetch=fetch)
            self.add_local_packages()
            return True


    """ Updates information of all packages """
    def update_package_info(self, fetch=True, local=False):
        if not local:
            return self.generate_package_info(fetch=fetch)

        with self.lock:
            self.remove_local_packages()
            self.add_local_packages()
            return True


    """ Gets packages from custom repositories found in the config file """
    def add_custom_repos(self, fetch=True):
        success = True

        if fetch:
            try:
                r = session.get(REPOLIST_URL, timeout=15)
                r.raise_for_status()
                repo_json = r.json()
                self.save_repo_file("repos.json", repo_json)
            except Exception as e:
                print_log(f"Could not get repository information.\n  {e}")
                success = False

        if not all([fetch, success]):
            repo_json = self.load_repo_file("repos.json")

        disabled_repos = CONFIG["disabled-repos"]
        local_repos = CONFIG["repos"] + disabled_repos

        repo_urls = []
        repo_urls.extend(repo_json.get("repos", []))
        repo_urls.extend(local_repos)

        for repo in repo_urls:
            if repo in self.repo_info:
                continue

            local = repo in local_repos
            disabled = repo in disabled_repos

            self.add_repo(repo, local=local, disabled=disabled)
            if self.repo_info[repo]["disabled"]:
                continue

            success = True

            if fetch:
                try:
                    r = session.get(repo, timeout=15)
                    r.raise_for_status()
                    repo_json = r.json()
                    self.repo_info[repo]["status"] = "Online"
                    self.save_repo_file(repo, repo_json, hashed=True)
                except Exception as e:
                    print_log(f"Could not get package information from {repo}.\n  {e}")
                    success = False

            if not all([fetch, success]):
                repo_json = self.load_repo_file(repo, hashed=True)

            self.repo_info[repo]["name"] = repo_json.get("name", "N/A")
            self.repo_info[repo]["version"] = get_float_version(repo_json.get("version", ""))
            self.repo_info[repo]["events"] = repo_json.get("events", {})

            plist = repo_json.get("packages", {})
            for package, pinfo in plist.items():
                self.add_package(package, pinfo)

        return True


    """ Gets packages from the local filesystem """
    def add_local_packages(self, description="Locally available content."):
        packs_dir = os.path.join(get_data_dir(), "packs")

        try:
            plist = next(os.walk(packs_dir))[1]
        except Exception as e:
            print_log(f"Could not get local packages information.\n  {e}")
            return False

        plist.sort()
        for package in plist:
            if self.package_exists(package):
                continue

            print_log(f"Adding local package: {package}")
            self.add_package(package, description=description, local=True)

        self.package_info["local"] = self.package_info.pop("local")
        return True


    """ Removes all local packages from the package list """
    def remove_local_packages(self):
        plist = []

        for package in self.package_info:
            if not self.package_info[package]["local"]:
                continue
            if package in self.special_packages:
                continue

            plist.append(package)

        for package in plist:
            print_log(f"Removing local package: {package}")
            self.package_info.pop(package, None)


    """ Adds a package to the package list """
    def add_package(self, package, pinfo={}, url="", description="", local=False):
        if self.package_exists(package):
            print_log(f"Package {package} already exists, skipping...")
            return False

        self.package_info[package] = {}
        self.package_info[package]["url"] = pinfo.get("url", url)
        self.package_info[package]["file"] = pinfo.get("file", self.get_package_file_name(package))
        self.package_info[package]["description"] = pinfo.get("description", description)
        self.package_info[package]["version"] = get_float_version(pinfo.get("version", ""))
        self.package_info[package]["checksum"] = pinfo.get("checksum", "")
        self.package_info[package]["local"] = local
        return True


    """ Returns a list of all available RVIO packages """
    def get_package_list(self, repo=PACKAGELIST_URL, fetch=True):
        self.add_repo(repo, name="Re-Volt I/O")

        success = True

        if fetch:
            try:
                self.is_connected = True
                r = session.get(repo, timeout=15)
                r.raise_for_status()
                self.repo_info[repo]["status"] = "Online"
                self.save_repo_file(repo, r.json())
                return r.json()
            except Exception as e:
                print_log(f"Could not fetch package list.\n  {e}")
                if isinstance(e, requests.ConnectionError):
                    self.is_connected = False
                success = False

        if not all([fetch, success]):
            return self.load_repo_file(repo)


    """ Loads a cached repository file """
    def load_repo_file(self, repo, hashed=False):
        fname = self.get_repo_file_name(repo, hashed)
        repo_path = os.path.join(CONFIG_DIR, "repos", fname)
        # Fall back to repos bundled with app
        if not os.path.isfile(repo_path):
            repo_path = os.path.join("repos", fname)
        try:
            with open(repo_path, "r") as f:
                return json.load(f)
        except Exception as e:
            print_log(f"Could not load repo file.\n  {e}")
            return {}


    """ Stores a repository file cache """
    def save_repo_file(self, repo, content, hashed=False):
        fname = self.get_repo_file_name(repo, hashed)
        repo_path = os.path.join(CONFIG_DIR, "repos", fname)
        try:
            with open(repo_path, "w") as f:
                json.dump(content, f, indent=4)
        except Exception as e:
            print_log(f"Could not save repo file.\n  {e}")


    """ Returns the repository file name from URL """
    def get_repo_file_name(self, repo, hashed=False):
        if hashed:
            return f"{hashed_string(repo)}.json"

        url_parsed = urlparse(repo)
        return os.path.basename(url_parsed.path)


    """ Adds a repository to the repository list """
    def add_repo(self, repo, name="N/A", local=False, disabled=False):
        if repo in self.repo_info:
            print_log(f"Repository {repo} already exists, skipping...")
            return False

        print_log(f"Adding repository: {repo}")
        self.repo_info[repo] = {}
        self.repo_info[repo]["url"] = repo
        self.repo_info[repo]["name"] = name
        self.repo_info[repo]["version"] = get_float_version("")
        self.repo_info[repo]["events"] = {}
        self.repo_info[repo]["status"] = "Offline"
        self.repo_info[repo]["local"] = local
        self.repo_info[repo]["disabled"] = disabled
        return True


    """ Adds a local repository to the repository list """
    def add_local_repo(self, repo):
        self.add_repo(repo, local=True, disabled=True)
        self.save_repo_list()


    """ Removes a list of local repositories from the repository list """
    def remove_local_repos(self, repo_list):
        for repo in repo_list:
            if repo not in self.repo_info:
                continue
            if not self.repo_info[repo]["local"]:
                continue

            print_log(f"Removing repository: {repo}")
            self.repo_info.pop(repo, None)

        self.save_repo_list()


    """ Enables or disables a locally added repository """
    def enable_repo(self, repo, enable=True):
        if repo not in self.repo_info:
            return
        if not self.repo_info[repo]["local"]:
            return

        self.repo_info[repo]["disabled"] = not enable
        self.save_repo_list()


    """ Writes list of repositories to the config file """
    def save_repo_list(self):
        CONFIG["repos"].clear()
        CONFIG["disabled-repos"].clear()

        for repo in self.repo_info:
            if not self.repo_info[repo]["local"]:
                continue

            if self.repo_info[repo]["disabled"]:
                CONFIG["disabled-repos"].append(repo)
            else:
                CONFIG["repos"].append(repo)

        save_config()


    """ Returns the package file path """
    def get_package_file(self, package):
        fname = self.package_info[package]["file"]
        return os.path.join(get_data_dir(), "downloads", fname)


    """ Returns the package destination folder """
    def get_package_dir(self, package):
        if package == "default":
            return get_data_dir()
        return os.path.join(get_data_dir(), "packs", package)


    """ Downloads and installs a package """
    def install_package(self, package):
        if close_event.is_set():
            return

        if not self.package_exists(package):
            print_log(f"Unable to locate package {package}")
            return

        unpack = not self.is_installed(package)
        if not unpack and self.is_outdated(package, "packs"):
            unpack = True

        if not unpack:
            print_log(f"{package} is already installed and up to date.")
            return

        download = not self.is_downloaded(package)
        if not download and self.is_outdated(package, "downloads"):
            download = True

        if download:
            print_log(f"Downloading {package}...")
            url = self.package_info[package]["url"]
            dl_file = self.get_package_file(package)
            resume = bool(self.package_info[package]["checksum"])

            # Download the file and save it to the data directory
            for p in download_file(url, dl_file, force=True, resume=resume):
                if p == -1:
                    print_log(f"Could not download {package}, skipping...")
                    return

                progress = (package, p)
                yield progress

            if not self.verify_package(package):
                print_log(f"Could not verify {package}, skipping...")
                return

            # Write the downloaded version number
            self.write_package_version(package, "downloads")

        print_log(f"Unpacking {package}...")
        if not self.unpack_package(package):
            print_log(f"Could not extract {package}, skipping...")
            return

        # Write the installed version number
        self.write_package_version(package, "packs")

        print_log("Successfully extracted.")


    """ Extracts a package to the package directory """
    def unpack_package(self, package):
        dl_file = self.get_package_file(package)

        data_dir = get_data_dir()
        if common.recipes["update"].has_package(package) and find_game(data_dir):
            extract_archive(dl_file, data_dir)

        out_dir = self.get_package_dir(package)
        clean_folder(out_dir)
        return extract_archive(dl_file, out_dir)


    """ Removes installed package files """
    def remove_package(self, package):
        if self.package_info[package]["local"]:
            print_log(f"Not removing local package {package}.")
            return

        print_log(f"Removing {package}...")
        package_dir = self.get_package_dir(package)
        if os.path.isdir(package_dir):
            remove_folder(package_dir)

        ver_file = os.path.join(get_data_dir(), "packs", f"{package}.ver")
        if os.path.isfile(ver_file):
            os.remove(ver_file)


    """ Removes downloaded package files """
    def clean_package(self, package):
        print_log(f"Cleaning {package}...")
        dl_file = self.get_package_file(package)
        if os.path.isfile(dl_file):
            os.remove(dload_file)

        ver_file = os.path.join(get_data_dir(), "downloads", f"{package}.ver")
        if os.path.isfile(ver_file):
            os.remove(ver_file)


    """ Returns a tuple containing valid content folders """
    def get_content_types(self):
        return ("cars", "levels", "cups", "gfx", "models", "music", "wavs", "strings")


    """ Returns the local content file path """
    def get_local_path(self, fname):
        if os.path.isabs(fname):
            return fname # path already represents the full path

        return os.path.join(get_data_dir(), "downloads", "local", fname)


    """ Downloads and installs a local content file """
    def install_local(self, fname, url="", download=False):
        path = self.get_local_path(fname)
        result = (path, False)

        if close_event.is_set():
            yield (None, result)
            return

        if download:
            for p in download_file(url, path, force=True, resume=False):
                if p == -1:
                    print_log(f"Could not download {fname}, skipping...")
                    yield (None, result)
                    return

                progress = (fname, p)
                yield (progress, result)

        if not validate_archive(path):
            message = {}
            message["title"] = "Warning"
            message["message"] = "\n".join((
                f"The archive '{fname}' doesn't contain a valid folder structure.",
                f"Do you really want to install it?"))
            message["response"] = False
            yield (None, message)

            if not message["response"]:
                yield (None, result)
                return

        local_dir = common.repo.get_package_dir("local")
        success = self.unpack_local(path, local_dir)

        result = (path, success)
        yield (None, result)


    """ Extracts a local content file to the package directory """
    def unpack_local(self, fname, fix_cases=True):
        stage_dir = self.get_local_path("staging")
        local_dir = common.repo.get_package_dir("local")

        create_folder(stage_dir)
        clean_folder(stage_dir)

        if not extract_archive(fname, stage_dir):
            return False

        success = merge_folders(stage_dir, local_dir, fix_cases)
        remove_folder(stage_dir)
        return success


    """ Gets the file name of a package from the url """
    def get_package_file_name(self, package):
        url = self.package_info[package]["url"]
        if url:
            url_parsed = urlparse(url)
            _, ext = os.path.splitext(url_parsed.path)
            return f"{package}{ext}"
        else:
            return f"{package}.zip"


    """ Writes a version file to keep track of the package version """
    def write_package_version(self, package, folder="packs"):
        if self.package_info[package]["local"]:
            return
        try:
            version = self.package_info[package]["version"]
            with open(os.path.join(get_data_dir(), folder, f"{package}.ver"), "w") as f:
                f.write(f"{version:.4f}")
        except Exception as e:
            print_log(f"Could not write version file.\n  {e}")
            return


    """ Gets the package version from the packs or downloads folder """
    def get_package_version(self, package, folder="packs"):
        if self.package_info[package]["local"]:
            return 0.0
        try:
            with open(os.path.join(get_data_dir(), folder, f"{package}.ver"), "r") as f:
                return float(f.readline().strip())
        except Exception:
            return 0.0


    """ Checks if a package download file exists """
    def verify_package(self, package):
        dl_file = self.get_package_file(package)
        checksum = self.package_info[package]["checksum"]
        return verify_file(dl_file, checksum)


    """ Checks if a package exists in the list of packages """
    def package_exists(self, package):
        return package in self.package_info


    """ Checks if a package is platform specific """
    def is_platform_package(self, package):
        return package in [f"rvgl_{p}" for p in PLATFORM_LIST]


    """ Checks if a package download file exists """
    def is_downloaded(self, package):
        dl_file = self.get_package_file(package)
        return os.path.isfile(dl_file)


    """ Checks if a package is installed """
    def is_installed(self, package):
        if self.package_info[package]["local"]:
            return True

        package_dir = self.get_package_dir(package)
        return os.path.isdir(package_dir)


    """ Checks if an installed or downloaded package is outdated """
    def is_outdated(self, package, folder="packs"):
        if folder == "downloads" and self.package_info[package]["checksum"]:
            return not self.verify_package(package)

        version = self.get_package_version(package, folder)
        return version < self.package_info[package]["version"]

