import os
import sys
from packaging.version import parse
from rv_launcher.common import *
from rv_launcher.helpers import *
from rv_launcher.version import __version__


""" Downloads and installs an updated version of the launcher """
def install_update():
    if os.path.isdir(".git"):
        print_log("Skipping update check in development repo.")
        return

    # Stage 1
    yield update_status("Checking for updates...")

    try:
        r = session.get(APPINFO_URL, timeout=15)
        r.raise_for_status()
        appinfo = r.json()
    except Exception as e:
        print_log(f"Could not fetch app info.\n  {e}")
        return

    version = appinfo.get("version", "")
    if parse(__version__) >= parse(version):
        print_log("RVGL Launcher is up to date.")
        return

    print_log(f"Update available. (v{version})")

    # Stage 2
    yield update_status("Downloading update...")

    fname = f"rvgl_launcher_{PLATFORM}.zip"
    dl_file = os.path.join(DATA_DIR, "downloads", fname)

    platform = appinfo.get(PLATFORM, {})
    url = platform.get("url", RVGL_URL + fname)
    checksum = platform.get("checksum", "")
    resume = bool(checksum)

    download = True
    if os.path.isfile(dl_file):
        local_version = read_update_version()
        if parse(local_version) >= parse(version):
            download = False
        if not verify_file(dl_file, checksum):
            download = True

    if download:
        for p in download_file(url, dl_file, force=True, resume=resume):
            if p == -1:
                print_log("Could not download update package.")
                return

        if not verify_file(dl_file, checksum):
            print_log("Could not verify update package.")
            return

        write_update_version(version)

    # Stage 3
    yield update_status("Unpacking update...")

    temp_dir = os.path.join(DATA_DIR, "update")
    clean_folder(temp_dir)

    if not extract_archive(dl_file, temp_dir):
        print_log("Could not unpack update package.")
        return

    # Waiting
    yield ""

    # Stage 4
    yield update_status("Installing update...")

    # Cannot replace a running application on Windows
    # but renaming should work
    if sys.platform == "win32":
        exefile = "rvgl_launcher.exe"
        bakfile = "rvgl_launcher.exe.bak"
        try:
            if os.path.isfile(bakfile):
                os.remove(bakfile)
            if os.path.isfile(exefile):
                os.rename(exefile, bakfile)
        except Exception as e:
            print_log(f"Could not create backup file.\n  {e}")
            return

    out_dir = os.getcwd()
    prepare_update(out_dir)
    copy_folder(temp_dir, out_dir)

    print_log("Successfully updated.")


""" Prepares the installation folder for the update """
def prepare_update(folder):
    # TODO: Consider auto-generating the list of files as a build step.
    files_list = [
        "__pycache__",
        "icons",
        "repos",
        "rv_launcher",
        "appdirs.py",
        "rvgl_launcher.py",
        "7z.dll",
        "7z.exe",
        "7za.exe",
        "helper.exe",
        "rvgl_launcher.exe"
    ]

    for entry in files_list:
        path = os.path.join(folder, entry)
        if os.path.isdir(path):
            remove_folder(path)
            continue
        try:
            os.remove(path)
        except Exception as e:
            continue


""" Print and return the update status message """
def update_status(message):
    print_log(message)
    return message


""" Writes a version file to keep track of the update package """
def write_update_version(version):
    try:
        ver_file = f"rvgl_launcher_{PLATFORM}.ver"
        with open(os.path.join(DATA_DIR, "downloads", ver_file), "w") as f:
            f.write(version)
    except Exception as e:
        print_log(f"Could not write version file.\n  {e}")
        return


""" Reads the version file of the downloaded update package """
def read_update_version():
    try:
        ver_file = f"rvgl_launcher_{PLATFORM}.ver"
        with open(os.path.join(DATA_DIR, "downloads", ver_file), "r") as f:
            return f.readline().strip()
    except Exception:
        return ""

