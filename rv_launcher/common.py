import glob
import sys
import os
import platform
import time
import threading
import requests
from appdirs import *


APP_NAME = "rvmm"
APP_TITLE = "RVGL Launcher"
APP_ID = "bd67d014" # unique identifier
APP_URI = "rvmm://"

DATA_DIR = user_data_dir(APP_NAME, False)
CONFIG_DIR = user_config_dir(APP_NAME, False)
PACKAGE_URL = "https://distribute.re-volt.io/packs/"
PACKAGELIST_URL = "https://distribute.re-volt.io/packages.json"
REPOLIST_URL = "https://distribute.re-volt.io/repos/"
RELEASE_URL = "https://distribute.re-volt.io/releases/"
RVGL_URL = "https://rvgl.re-volt.io/downloads/"
RVGL_VERSION_URL = RELEASE_URL + "rvgl_version.txt"
APPINFO_URL = RVGL_URL + "rvgl_launcher.json"

RVIO_URL = "https://re-volt.io/"
RVIO_EVENTS_URL = RVIO_URL + "events-data"

CONFIG = {
    "install-type": "standard",
    "data-dir": DATA_DIR,
    "save-dir": "",
    "launch-dir": "",
    "launch-params": "",
    "installed": False,
    "uri-registered": False,
    "repos": [],
    "disabled-repos": [],
    "install-recipe": "original",
    "default-recipe": "default",
    "show-console": False
}

"""Returns a dict containing folder names of the tracks as keys and all tracknames found for that key as list value"""


def get_track_folder_dict():
    track_folder_dict = {}
    pack_dir = (os.path.join(DATA_DIR, "packs"))
    files_depth_3 = glob.glob(os.path.join(pack_dir, '*/*/*'))
    dirs_depth_3 = filter(lambda f: os.path.isdir(f) and "levels" in f, files_depth_3)  # find all levels
    for _ in dirs_depth_3:
        for track_dir in list(dirs_depth_3):
            name = os.path.split(track_dir)[-1] # get track folder name
            filename = name + ".inf"
            try:
                with open(os.path.join(track_dir, filename)) as track_inf_file:
                    for line in track_inf_file:
                        if "NAME" in line:
                            if name not in track_folder_dict:  # make a list as value, because you could have different versions of the track installed
                                track_folder_dict[name] = []
                            track_folder_dict[name].append(line.split(";")[0].replace("NAME", "").replace(" ", "").replace("'", "").replace("\"", "").replace("\t",
                                                                                                                                                              ""))  # read track name from inf file, remove all spaces and write it to the dict
                            break  # name is found go on with next
            except FileNotFoundError:  # Skip directories like io loadlevel
                pass
    return track_folder_dict


CUP_MAX_TRACK_COUNT = 16
TRACK_FOLDER_NAME_DICT = get_track_folder_dict()

PLATFORM_LIST = ["win32", "win64", "linux"]

if sys.platform == "linux":
    PLATFORM = "linux"
elif sys.platform == "win32":
    if platform.architecture()[0] == "64bit":
        PLATFORM = "win64"
    else:
        PLATFORM = "win32"
else:
    print("Unsupported platform.")
    exit()


repo = None
event_catalog = None
log_file = None
recipes = {}

close_event = threading.Event()
close_event.clear()

message_event = threading.Event()
message_event.clear()

session = requests.Session()


""" Returns whether application is run from a frozen bundle """
def is_frozen():
    return getattr(sys, "frozen", False)


""" Returns the application base folder """
def get_app_path():
    if is_frozen():
        return os.path.dirname(sys.executable)
    else:
        return sys.path[0]


""" Returns the path to assets bundled with the application """
def get_dist_path():
    if sys.platform == "win32" and not is_frozen():
        return os.path.join("dist", PLATFORM)
    else:
        return ""


""" Returns the command and args used to launch the application """
def get_app_command():
    if is_frozen():
        return sys.executable, ""
    else:
        name = os.path.join(get_app_path(), "rvgl_launcher.py")
        return sys.executable, name

